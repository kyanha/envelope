﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Asn1;

namespace ClericalStorage
{
    #region public Stamp
    /// <summary>
    /// Non-secured Assertion stamp
    /// </summary>
    /// Stamp ::= Assertion
    /// 
    /// Assertion ::= SEQUENCE {
    ///	    assertionInternalID INTEGER OPTIONAL,
    ///     assertionName UTF8String OPTIONAL,
    ///     assertionType [7] AssertionType,
    ///     assertionContent OCTET STRING }

    public class Stamp : Assertion
    {
        public Stamp(Asn1Encodable i) : base(i)
        {
        }
    }
    #endregion
}
