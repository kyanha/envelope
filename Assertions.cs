﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.X509;

namespace ClericalStorage
{
    #region public Assertions
    /// <summary>
    /// Sequence of individual Assertion
    /// </summary>
    /// <remarks>
    /// Assertions ::= SEQUENCE(1..MAX) OF Assertion
    /// </remarks>
    public class Assertions:Deserializable
    {
        public List<Assertion> data = new List<Assertion>();
        public Stream assertionsOutputStream = new MemoryStream();
        public byte[] assertionsOctetVector;
        public Chop outerChop;
        public Envelope myEnvelope;

        internal void Add(Assertion input)
        {
            if (input == null)
                throw new ArgumentNullException("input");
            if (!(input is Assertion))
                throw new ArgumentException("Assertions: Expected an Assertion?");
            if (input.assertionType == new AssertionType("application/x509-certificate"))
                myEnvelope.AddRequiredProof(input);
            data.Add(input);
        }

        internal Assertions()
        {
        }

        public Assertions(Asn1Encodable asn1Encodable) : base(asn1Encodable)
        {
            Deserialize(asn1Encodable);
        }

        internal Stream Serialize()
        {
            if (data.Count == 0)
                return null;                                
            var o = new DerSequenceGenerator(assertionsOutputStream);

            foreach (var i in data)
                o.AddObject(i.ToAsn1Encodable());
            o.Close();
            assertionsOutputStream.Position = 0;
            assertionsOctetVector = new byte[assertionsOutputStream.Length];
            assertionsOutputStream.Read(assertionsOctetVector, 0, assertionsOctetVector.Length);
            return assertionsOutputStream;
        }

        internal Asn1Encodable ToAsn1Encodable()
        {
            return Asn1Sequence.FromStream(Serialize());
        }

        internal byte[] ToByteArray()
        {
            var s = Serialize();
            byte[] output = new byte[s.Length];
            System.Diagnostics.Debug.Assert(output.Length == s.Length);
            s.Read(output, 0, output.Length);
            return output;
        }

        public override void Deserialize(Asn1Encodable encodableAssertions)
        {
            var sequence = Asn1Sequence.GetInstance(encodableAssertions);
            if (sequence == null)
                throw new ArgumentException("Assertions: Deserialization didn't get an Asn1Sequence");
            foreach (Asn1Encodable m in sequence)
            {
                var n = new Assertion(m);
                n.SetEnvelope(myEnvelope);
            }
        }

        public bool Valid { get { return (data.Count != 0); } }
    }
    #endregion
}
