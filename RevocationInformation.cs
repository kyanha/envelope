﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClericalStorage
{
    /// <summary>
    /// Revocation information for a certificate.
    /// </summary>
    public class RevocationInformation
    {
        public static IList<Assertion> assertionsToProcess;
        public static IList<Assertion> assertionsProcessed;
    }
}
