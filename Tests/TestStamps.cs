﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClericalStorage.Tests
{
    /// <summary>
    /// Tests the Stamps functionality (add and remove from the collection)
    /// </summary>
    class TestStamps 
    {
        ClericalStorage.Stamps myStamps;

        bool RunTest()
        {
            myStamps = new ClericalStorage.Stamps();

            myStamps.Add(new ClericalStorage.Assertion(null,
                new byte[] {03,03,03},
                new ClericalStorage.AssertionType("application/x-ignored-data"),
                String.Empty,null));

            var encoded = myStamps.ToByteArray();

            var unencoded = new ClericalStorage.Stamps(encoded);

            if (unencoded.data.Count != 1)
                throw new Exception("Count " + unencoded.data.Count + " not 1");

            return true;
        }

    }
}
