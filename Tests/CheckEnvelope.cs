﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClericalStorage.Tests
{
    class CheckEnvelope
    {
        static CheckEnvelope application;

        private ClericalStorage.Envelope myEnvelope;

        static CheckEnvelope() { }
        public CheckEnvelope()
        {

            myEnvelope = new ClericalStorage.Envelope();

            Assertion a;
            Environment.CurrentDirectory = @"c:\users\kyanha\work\sandbox";

            int count = 0;

            //System.Diagnostics.Debugger.Break();

            foreach (var i in System.IO.Directory.GetFiles(Environment.CurrentDirectory, "*.pem", System.IO.SearchOption.TopDirectoryOnly))
            {
                var m = System.IO.File.Open(i, System.IO.FileMode.Open);
                var t = new System.IO.StreamReader(m, new System.Text.ASCIIEncoding());
                var n = new Org.BouncyCastle.Utilities.IO.Pem.PemReader(t);
                var p = n.ReadPemObject().Content;
                a = new Assertion();

                a.SetContent(new System.IO.MemoryStream(p));
                myEnvelope.Add(a);

                var certparser = new Org.BouncyCastle.X509.X509CertificateParser();
                var cert = certparser.ReadCertificate(p);

                myEnvelope.AddRecipient(new AsymmetricIdentity(cert.GetPublicKey()));
                if (count++ > 5)
                    break;
            }
            var b = myEnvelope.Seal();
            var s = new System.IO.MemoryStream(b);
            Console.WriteLine("serialized");
            var s2 = new System.IO.FileStream("basicstructure.der", System.IO.FileMode.Truncate, System.IO.FileAccess.ReadWrite);
            s.CopyTo(s2);
            s2.Close();

            System.Console.ReadKey();
        }

        static void Main(string[] mine)
        {
            application = new CheckEnvelope();
        }
    }
}
