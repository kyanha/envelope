﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Org.BouncyCastle.Asn1;

namespace ClericalStorage
{
    #region public Postmarks
    /// <summary>
    /// Sequence of Postmark
    /// </summary>
    /// <remarks>
    /// Postmarks ::= SEQUENCE (1..MAX) OF Postmark
    /// </remarks>
    internal class Postmarks
    {
        List<Postmark> data = new System.Collections.Generic.List<Postmark>();
        Stream streamToPostmark;

        internal Stream Serialize()
        {
            // This is where we need to have the list of hashes we want
            string[] myAlgorithms = new string[] {
                "SHA-1",
                "SHA-512",
                "RIPEMD160",
                "GOST3411"
            };
            // Add the Postmark for each algorithm to the data
            foreach (var s in myAlgorithms)
            {
                data.Add(new Postmark(s, streamToPostmark));
            }

            // Now, we add these to a sequence...

            MemoryStream outputStream = new MemoryStream();
            var dsg = new DerSequenceGenerator(outputStream);

            foreach (var i in data)
            {
                streamToPostmark.Position = 0;
                dsg.AddObject(i.toAsn1Encodable());
            }
            dsg.Close();
            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }

        internal Asn1Encodable ToAsn1Encodable()
        {
            return Asn1Sequence.FromStream(Serialize());
        }

        internal Postmarks(Stream input)
        {
            streamToPostmark = input;
        }

        public Postmarks(Asn1Encodable asn1Encodable)
        {
            var postmarkSequence = Asn1Sequence.GetInstance(asn1Encodable);
            foreach (var sequenceEntry in postmarkSequence)
            {
                data.Add(Postmark.GetInstance(sequenceEntry));
            }
        }
    }
    #endregion
}
