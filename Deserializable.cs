﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Asn1;

namespace ClericalStorage
{
    public abstract class Deserializable
    {
        public Deserializable()
        {
            fromEnvelope = false;
        }
        public Deserializable(Org.BouncyCastle.Asn1.Asn1Encodable value)
        {
            fromEnvelope = true;
        }
        abstract public void Deserialize(Asn1Encodable value);
        public bool fromEnvelope { get; internal set; }
    }

}
