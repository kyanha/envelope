﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Asn1;

namespace ClericalStorage
{
    /// <summary>
    /// Array of LetterOpener
    /// </summary>
    /// <remarks>
    /// StrongBox ::= SEQUENCE(0..MAX) OF LetterOpener
    /// </remarks>
    internal class StrongBox : Deserializable
    {
        List<LetterOpener> data;

        public ICollection Collection { get { return data; } }

        internal void Add(LetterOpener LetterOpener)
        {
            this.data.Add(LetterOpener);
        }

        internal void AddRecipient(AsymmetricIdentity value)
        {
            data.Add(new LetterOpener(recipient: value));
        }

        internal void SetSymKey(byte[] keyTBProtected)
        {
            this.SymKey = keyTBProtected;
        }

        /// <summary>
        /// created by OpaquePayload and passed in
        /// </summary>
        public byte[] SymKey { internal get; set; }

        /// <summary>
        /// created by Envelope and passed in 
        /// </summary>
        public Chop OuterChop { internal get; set; }

        public void SetOuterChop(Chop value) { OuterChop = value; }

        public Stream Serialize()
        {
            // TODO: Remove after debugging the multi-recipient
            System.Diagnostics.Debug.Assert(data.Count > 0);
            if ((data.Count == 0) && (SymKey != null))
                throw new InvalidOperationException("have symkey but no recipients");
            var stream = new MemoryStream();
            // TODO: Remove after debugging recipients
            System.Diagnostics.Debug.Assert(SymKey != null);
            if (SymKey == null)
            {
                // If SymKey is null, then we're in a context where there are no recipients
                // Should return NULL for it, so we return DerNull.Instance.
                stream.Write(DerNull.Instance.GetDerEncoded(), 0, DerNull.Instance.GetDerEncoded().Length);
                stream.Seek(0, System.IO.SeekOrigin.Begin);
                return stream;
            }

            var sg = new Org.BouncyCastle.Asn1.DerSequenceGenerator(stream);
            foreach (LetterOpener sbi in data)
            {
                sbi.SetSymmetricKey(this.SymKey);
                sbi.SetOuterChop(this.OuterChop);
                sg.AddObject(sbi.toAsn1Encodable());
            }
            sg.Close();
            stream.Seek(0, SeekOrigin.Begin);
            return stream;
        }

        public Asn1Encodable ToAsn1Encodable()
        {
            return Asn1Object.FromStream(Serialize());
        }

        public bool Valid { get; set; }

        public StrongBox()
        {
            data = new List<LetterOpener>();
        }

        public StrongBox(byte[] value): base(Asn1Object.FromByteArray(value))
        {
            Deserialize(Asn1Object.FromByteArray(value));
        }

        public override void Deserialize(Asn1Encodable value)
        {
            var sequence = Asn1Sequence.GetInstance(value);
            foreach (var letterOpener in sequence)
                data.Add(new LetterOpener(letterOpener as Asn1Encodable));
        }
    }
}
