﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Crypto.Signers;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Crypto.Digests;
using Org.BouncyCastle.X509;
using Org.BouncyCastle.Crypto;

namespace ClericalStorage
{
    #region public Proof
    /// <summary>
    /// A Chop's signature over AssertionCommitment
    /// </summary>
    /// <remarks>
    /// Proof ::= SEQUENCE {
    ///       -- the signature algorithm used
    ///     signatureAlgorithm AlgorithmIdentifier,
    ///     keyIdentifier OCTET STRING OPTIONAL,
    ///       -- over AssertionCommitment
    ///     signature OCTET STRING }
    /// 	
    /// A Proof is the product of one and only one Chop.  keyIdentifier is
    /// optional because an interface will typically only have one keypair
    /// that matches a given v6mcaddr:port combination, and it makes little
    /// sense to expose any information if there is any means to avoid it.
    /// </remarks>
    public class Proof : Deserializable
    {
        #region Members

        Stream streamToDigest;
        byte[] keyIdentifier;
        byte[] nomineeSignature;
        Org.BouncyCastle.Crypto.ISigner mySigner;
        Chop myChop;

        #endregion
        #region Nominated Proof
        public Proof(Asn1Encodable proof)
            : base(proof)
        {
            Deserialize(proof);
        }

        /// <summary>
        /// Deserialization constructor
        /// </summary>
        /// <param name="proof">The Asn1Sequence that contains the proof to verify</param>
        /// <param name="keyIdentifier">The key ID of the public key to verify against</param>
        public Proof(Asn1Encodable proof, byte[] keyIdentifier)
            : base(proof)
        {
            Deserialize(proof);
            SetKeyIdentifier(keyIdentifier);
        }

        public void SetKeyIdentifier(byte[] keyIdentifier)
        {
            this.keyIdentifier = keyIdentifier;
            this.myChop = KeyStore.FindKnownVerifier(keyIdentifier);
        }

        public void SetStreamToDigest(Stream readableStream)
        {
            if (readableStream == null) throw new ArgumentNullException("readableStream");
            if (!readableStream.CanRead || !readableStream.CanSeek)
                throw new ArgumentException("Proof: Stream to digest must be readable and seekable");
            this.streamToDigest = readableStream;
        }

        public bool isReadyToVerify
        {
            get
            {
                return (this.streamToDigest is Stream && this.streamToDigest.CanRead
                    && this.keyIdentifier != null && KeyStore.FindKnownVerifier(this.keyIdentifier) != null);
            }
        }
        
        public bool Verify()
        {
            // At this point, we should know the keyIdentifier and the deserialized Proof.
            // We need the stream to digest to be set.
            if (!(this.streamToDigest is Stream) || !this.streamToDigest.CanRead || !this.streamToDigest.CanSeek)
                throw new InvalidOperationException("Proof: Verify: must have a readable, seekable Stream");
            if (!isReadyToVerify)
                throw new InvalidOperationException("Proof: requires stream to digest and a nominee key identifier");
            this.streamToDigest.Position = 0;

            mySigner.Init(false, getPublicKeyFromIdentifier(this.keyIdentifier));

            throw new NotImplementedException();
        }

        private ICipherParameters getPublicKeyFromIdentifier(byte[] p)
        {
            throw new NotImplementedException();
        }

        public override void Deserialize(Asn1Encodable proof)
        {
            var seq = Asn1Sequence.GetInstance(proof);
            if (!(seq is Asn1Sequence) || seq.Count < 2 || seq.Count > 3)
                throw new ArgumentException("proof","Proof: " + seq == null ? "(null)" : seq.Count + " is out of bounds (<= 2 || >= 3)");
            var seqList = new List<Asn1Encodable>();
            foreach (var m in seq)
                seqList.Add(m as Asn1Encodable);
            this.mySigner = Org.BouncyCastle.Security.SignerUtilities.GetSigner(DerObjectIdentifier.GetInstance(seqList[0]));
            seqList.RemoveAt(0);
            if (seqList.Count == 2)
                this.SetKeyIdentifier(DerOctetString.GetInstance(seqList[0]).GetOctets());
            seqList.RemoveAt(0);
            nomineeSignature = seqList[0].GetDerEncoded();
        }
        #endregion
        #region New Proof
        internal Proof(Org.BouncyCastle.Crypto.ISigner thisSigner, Stream input, Chop thisChop)
        {
            if (!(thisSigner is ISigner))
                throw new ArgumentNullException("hash","Proof: signature algorithm is incomprehensible");
            if (!(input is Stream) || !input.CanSeek || !input.CanRead)
                throw new ArgumentException("Proof: input stream must be readable and seekable");
            if (!(thisChop is Chop))
                throw new ArgumentException("Proof: Chop must not be null");
            mySigner = thisSigner;
            streamToDigest = input;
            EquipChop(thisChop);
        }

        internal void EquipChop(Chop input)
        {
            if (input == null)
                throw new ArgumentNullException("input");
            if (input.verifyOnly)
                throw new ArgumentException("input", "Proof: Trying to equip a verify-only chop?");
            myChop = input;
        }

        internal Stream Serialize()
        {
            if (myChop == null)
                throw new InvalidOperationException("no Chop equipped");
            if (myChop.verifyOnly)
                throw new InvalidOperationException("Current Chop is only for verification");
            Stream outputStream = new MemoryStream();
            var outputSequenceGenerator = new DerSequenceGenerator(outputStream);

            // BUGBUG: I fully expect this to blow up.  I need to get the right OID
            // for the signatureAlgorithm, which is based upon the asymmetric algorithm
            // and the digest.

            // Add object 1: signature AlgorithmIdentifer
            outputSequenceGenerator.AddObject(
                Org.BouncyCastle.Security.SignerUtilities.GetObjectIdentifier(
                    mySigner.AlgorithmName));

            // Add object 2: keyIdentifer for the Chop
            IDigest myDigest = Config.Configuration.keyIdentifierDigest;
            byte[] keyIdentifier = new byte[myDigest.GetByteLength()];
            var mySPKI = SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo(myChop.publicKey);

            myDigest.BlockUpdate(mySPKI.GetDerEncoded(),0,mySPKI.GetDerEncoded().Length);
            
            myDigest.DoFinal(keyIdentifier, 0);

            outputSequenceGenerator.AddObject(new DerOctetString(keyIdentifier));

            // Add final object: actual signature.
            byte[] arrayToDigest = new byte[streamToDigest.Length];
            streamToDigest.Read(arrayToDigest, 0, arrayToDigest.Length);
            mySigner.BlockUpdate(arrayToDigest, 0, arrayToDigest.Length);

            mySigner.Init(true, myChop.privateKey);
            byte[] signatureoutput = mySigner.GenerateSignature();

            
            outputSequenceGenerator.AddObject(
                new Org.BouncyCastle.Asn1.DerOctetString(signatureoutput));
            outputSequenceGenerator.Close();
            outputStream.Position = 0;
            return outputStream;
        }


        internal Asn1Encodable ToAsn1Encodable()
        {
            return Asn1Object.FromStream(Serialize());
        }

        #endregion

    }
    #endregion
}
