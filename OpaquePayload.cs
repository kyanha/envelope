﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security.Certificates;
using System.IO;

namespace ClericalStorage
{
    /// <summary>
    /// Payload after symmetric encryption (possibly with Null algorithm)
    /// </summary>
    /// <remarks>
    /// OpaquePayload ::= SEQUENCE {
    ///     symmetricAlgorithm AlgorithmIdentifier,
    ///     opaquePayload OCTET STRING }
    ///     
    /// OpaquePayload can be thought of as a "shipping container", which holds
    /// and optionally (usually) obscures the Payload.  (Apologies for mixing
    /// multiple shipping analogies, but it's rather difficult.)  It holds the
    /// algorithm OID and the data encrypted with that identified algorithm.
    /// The symmetric key for the transform is stored in the StrongBox
    /// extension, in one of the LetterOpeners.
    /// 
    /// This really needs to expose AddPayload.
    /// </remarks>
    internal class OpaquePayload : Deserializable, IAddPayload
    {
        /// <summary>
        /// The intended recipients (public keys)
        /// </summary>
        public List<AsymmetricIdentity> recipientList = new List<AsymmetricIdentity>();
        public Chop outermostChop;
        internal byte[] symmetricSecret { private get; set; }
        private Asn1Sequence OpaquePayloadData;
        private Asn1OctetString opaquePayload;

        /// <summary>
        /// The non-opaque stamp of the transmission
        /// </summary>
        public Payload payload;

        public Envelope myEnvelope { get; internal set; }

        [Obsolete("An OpaquePayload requires a StrongBox",true)]
        internal OpaquePayload()
        {
            throw new NotImplementedException();
        }

        internal StrongBox strongbox;

        internal void AddRecipient(AsymmetricIdentity recipient)
        {
            recipientList.Add(recipient);
        }

        internal OpaquePayload(StrongBox sbox)
        {
            strongbox = sbox;
            payload = new Payload();
        }

        internal bool isEncrypted { get { return (!myAlgorithm.Equals(TheClerkObjectIdentifiers.idNullAlgorithm)); } }
        private DerObjectIdentifier myAlgorithm4Real;
        public DerObjectIdentifier myAlgorithm
        {
            get
            {
                return myAlgorithm4Real ?? new DerObjectIdentifier("2.23.133.4.1");
            }
            private set
            {
                myAlgorithm4Real = value;
            }
        }

        internal bool isReady
        {
            get
            {
                // If there's no data, it's not ready.
                if (payload == null) return false;
                // If the algorithm isn't Null, make sure there's a generated secret
                if (myAlgorithm != TheClerkObjectIdentifiers.idNullAlgorithm)
                    if (symmetricSecret == null) return false;
                return true;
            }
        }

        // Constructor from a pre-existing OpaquePayload structure
        // Argument must be an Asn1Sequence
        public OpaquePayload(byte[] value)
        {
            Deserialize(Asn1Object.FromByteArray(value));
        }

        public override void Deserialize(Asn1Encodable value)
        {
            // Top-level extension, so the Asn1Encodable is a DerOctetString
            // containing an Asn1Sequence.
            OpaquePayloadData = Asn1Sequence.GetInstance(value);
            if (OpaquePayloadData == null)
                throw new CertificateException("OpaquePayload is not a sequence");
            if (OpaquePayloadData.Count != 2)
                throw new CertificateException("OpaquePayload sequence length " + OpaquePayloadData.Count + " not 2");
            var a = OpaquePayloadData[0];
            opaquePayload = Asn1OctetString.GetInstance(OpaquePayloadData[1]);

            if (DerObjectIdentifier.GetInstance(a) == TheClerkObjectIdentifiers.idNullAlgorithm)
            // this is NULL encryption, so we can just make the Payload from index 1 and return
            {
                payload = new Payload(Asn1Sequence.GetInstance(OpaquePayloadData[1]));
                opaquePayload = Asn1OctetString.GetInstance(OpaquePayloadData[1]);
                myAlgorithm4Real = TheClerkObjectIdentifiers.idNullAlgorithm;
                return;
            }

            // It's encrypted, so we have to go to the StrongBox to get the key.
            // This can't be done from within the OpaquePayload constructor, so
            // we must have another method which loads the key.
        }

        public Stream Serialize()
        {
            // If recipientList is empty, there is no need to create the
            // OpaquePayload -- the OpaquePayload is essentially just the
            // unencrypted Payload.  If recipientList is empty, strongbox.SymKey
            // is set to null, and OpaquePayload serializes unencrypted.
            //
            // The "null" encryption algorithm 
            // is used when there'outputStream no particular desire or capacity for encryption,
            // such as when the Envelope is used to carry multiple certificate
            // chains for a single website.

            // Need to serialize Payload
            byte[] payload = this.payload.ToByteArray();

            // If recipient list is non-empty, generate the symmetric secret
            if (recipientList.Count > 0)
            {
                var bitgen = new Org.BouncyCastle.Crypto.Prng.CryptoApiRandomGenerator();
                var sr = new Org.BouncyCastle.Security.SecureRandom(bitgen);
                KeyGenerationParameters kg = new KeyGenerationParameters(sr,
                    192);  // AES192
                CipherKeyGenerator ckg = new CipherKeyGenerator();
                ckg.Init(kg);
                symmetricSecret = ckg.GenerateKey();
                strongbox.SetSymKey(symmetricSecret);
                Console.WriteLine("OpaquePayload: Generated the secret key");
            }
            else
            {
                strongbox.SetSymKey(null);
                Console.WriteLine("OpaquePayload: No recipients, OpaquePayload is not opaque");
            }

            byte[] opaqueOutput;

            var outStream = new MemoryStream();
            var dsg = new Org.BouncyCastle.Asn1.DerSequenceGenerator(outStream);

            // Now that we have the key, we need to encrypt.  We're using AES-192.
            // TODO: make this configurable in Envelope.Config.Configuration
            // (or app property)
            if (strongbox.SymKey != null)
            {
                var cipherImplementation = Org.BouncyCastle.Security.CipherUtilities.GetCipher(
                    Config.Configuration.OpaquePayloadBlockCipher.Id);
                cipherImplementation.Init(true, new Org.BouncyCastle.Crypto.Parameters.KeyParameter(strongbox.SymKey));

                opaqueOutput = cipherImplementation.DoFinal(payload);
                dsg.AddObject(Org.BouncyCastle.Security.CipherUtilities.GetObjectIdentifier(cipherImplementation.AlgorithmName));
                dsg.AddObject(new Org.BouncyCastle.Asn1.DerOctetString(opaqueOutput));
            }
            else
            {
                opaqueOutput = payload;
                dsg.AddObject(TheClerkObjectIdentifiers.idNullAlgorithm);
                dsg.AddObject(new Org.BouncyCastle.Asn1.DerOctetString(opaqueOutput));
            }
            dsg.Close();
            outStream.Seek(0, SeekOrigin.Begin);
            return outStream;
        }

        internal byte[] AsByteArray()
        {
            var s = Serialize();
            var tmp = new byte[s.Length];
            s.Read(tmp, 0, tmp.Length);
            return tmp;
        }

        public Asn1Encodable ToAsn1Encodable()
        {
            return Asn1Object.FromStream(Serialize());
        }

        internal void SetOuterChop(AsymmetricCipherKeyPair keypair)
        {
            SetOuterChop(new Chop(keypair));
        }

        internal void SetOuterChop(Chop chop)
        {
            outermostChop = chop;

            var spki = Org.BouncyCastle.X509.SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo(chop.identityChop.Public);
            var assertion = new Assertion(null, spki.GetDerEncoded(), new AssertionType("application/x-envelope-spki"), null, null);
            this.strongbox.SetOuterChop(chop);
            payload.AddAssertion(assertion);
            payload.AddChop(chop);
        }

        internal void AddChop(Chop equipment)
        {
            payload.AddChop(equipment);
        }

        internal void AddAssertion(Assertion a)
        {
            payload.AddAssertion(a);
        }

        public bool Valid { get { return isReady; } }

        internal void AddRequiredProof(Assertion input)
        {
            payload.AddRequiredProof(input);

        }

        #region IAddPayload Members

        public void AddPayload(object payload)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
