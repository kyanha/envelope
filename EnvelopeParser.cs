﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Asn1;
using X509 = Org.BouncyCastle.X509;

namespace ClericalStorage
{
    /// <summary>
    /// Parse an Envelope for its stamp, verifying it along the way
    /// </summary>
    public class EnvelopeParser
    {
        // The Certificate which contains the Envelope
        private Org.BouncyCastle.X509.X509Certificate myCertificate;
        // The Certificate Parser we need to use
        private Org.BouncyCastle.X509.X509CertificateParser myParser =
            new Org.BouncyCastle.X509.X509CertificateParser();

        public EnvelopeParser(byte[] value)
        {
            myCertificate = myParser.ReadCertificate(value);
            initialize(myCertificate);
        }

        public EnvelopeParser(System.IO.Stream input)
        {
            myCertificate = myParser.ReadCertificate(input);
            initialize(myCertificate);
        }

        public EnvelopeParser(Org.BouncyCastle.X509.X509Certificate value)
        {
            initialize(value);
        }

        private void initialize(Org.BouncyCastle.X509.X509Certificate value)
        {
            bool haveAtLeastOne = false;
            var tbsCertificate = Org.BouncyCastle.Asn1.X509.TbsCertificateStructure.GetInstance(value.GetTbsCertificate());
            var certificateExtensions = tbsCertificate.Extensions;
            var extensionOIDs = certificateExtensions.GetExtensionOids();
            System.Diagnostics.Debug.Print("OIDs present?");
            foreach (var extensionIdentifier in extensionOIDs)
            {
                switch (extensionIdentifier.Id)
                {
                    case "1.3.6.1.4.1.22232.7.1.0.0":
                        haveAtLeastOne = true;
                        System.Diagnostics.Debug.Print(" OpaquePayload");
                        break;

                    case "1.3.6.1.4.1.22232.7.1.0.1":
                        haveAtLeastOne = true;
                        System.Diagnostics.Debug.Print(" StrongBox");
                        break;

                    case "1.3.6.1.4.1.22232.7.1.0.2":
                        haveAtLeastOne = true;
                        System.Diagnostics.Debug.Print(" Copyright");
                        break;

                    case "1.3.6.1.4.1.22232.7.1.0.3":
                        haveAtLeastOne = true;
                        System.Diagnostics.Debug.Print(" Stamps");
                        break;
                }
            }
            if (!haveAtLeastOne)
                throw new Org.BouncyCastle.Security.Certificates.CertificateParsingException(
                    "EnvelopeParser: passed a non-envelope");
            System.Diagnostics.Debug.WriteLine("");
            System.Console.WriteLine();
//            System.Console.WriteLine("Hit a key...");
//            System.Console.ReadKey();

            // Okay, we know that this is good.  Now to verify that we're not parsing something we shouldn't...
            var n = certificateExtensions.GetCriticalExtensionOids();
            System.Diagnostics.Debug.Print("Critical Extensions:");
            foreach (var o in n)
            {
                switch (o.Id)
                {
                    case "1.3.6.1.4.1.22232.7.1.0.0":
                        System.Diagnostics.Debug.Print(" opaquePayload");                        
                        break;
                    case "1.3.6.1.4.1.22232.7.1.0.1":
                        System.Diagnostics.Debug.Print(" strongBox");
                        break;
                    case "1.3.6.1.4.1.22232.7.1.0.2":
                        System.Diagnostics.Debug.Print(" copyrightLicense");
                        break;
                    case "1.3.6.1.4.1.22232.7.1.0.3": 
                        System.Diagnostics.Debug.Print(" Stamps"); break;
                    default:
                        throw new Org.BouncyCastle.Security.Certificates.CertificateException(
                            "Unrecognized critical extension OID: " + o.Id);
                }
            }
            System.Diagnostics.Debug.WriteLine("");
            extensionOIDs = certificateExtensions.GetExtensionOids();
            Console.WriteLine("Non-Critical Extensions:");
            foreach (var o in extensionOIDs)
            {
                Console.WriteLine(o.Id);
            }
            Console.WriteLine();
            // If we've gotten here, we know that we're dealing with an Envelope structure.

            var opaquePayload = certificateExtensions.GetExtension(new DerObjectIdentifier("1.3.6.1.4.1.22232.7.1.0.0")).Value.GetOctets();
            var strongBox = certificateExtensions.GetExtension(new DerObjectIdentifier("1.3.6.1.4.1.22232.7.1.0.1")).Value.GetOctets();
            var copyrightLicense = certificateExtensions.GetExtension(new DerObjectIdentifier("1.3.6.1.4.1.22232.7.1.0.2")).Value.GetOctets();
            var stamps = certificateExtensions.GetExtension(new DerObjectIdentifier("1.3.6.1.4.1.22232.7.1.0.3")).Value.GetOctets();

            // First, we need to examine the copyrightLicense.
            // TODO parse the copyrightLicense

            CopyrightLicense myLicense = new CopyrightLicense(copyrightLicense);

            // Next, we display the Stamps.
            Stamps myStamps = new Stamps(stamps);


            var oPayload = new OpaquePayload(opaquePayload);

            // Figure out if OpaquePayload is encrypted
            // if it is, must load strongBox and find recipient key
            // (though it might just be in there unencrypted, also a possibility)
            // so, oPayload has an algorithm and needs a key.  It'll wait until
            // the strongBox is parsed
            StrongBox sBox;
            if (oPayload.myAlgorithm != TheClerkObjectIdentifiers.idNullAlgorithm)
                sBox = new StrongBox(strongBox);

            // And this just for ease of test harness:
            System.Console.WriteLine("Hit another key, please...");
            System.Console.ReadKey();
        }

        List<Org.BouncyCastle.X509.X509Certificate> certificateUniverse =
            new List<Org.BouncyCastle.X509.X509Certificate>();
        List<AsymmetricIdentity> keysToProve = new List<AsymmetricIdentity>();
    }
}
