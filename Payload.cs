﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Asn1;
using System.IO;
using Org.BouncyCastle.Security;


namespace ClericalStorage
{

    /// <summary>
    /// The data the Envelope protects for transmission, its timestamps, and its proofs
    /// </summary>
    /// <remarks>
    /// Payload ::= SEQUENCE {
    ///     stamp EnvelopeContent,
    ///     postmark Postmarks }
    /// </remarks>
    public class Payload : Deserializable
    {
        // includes the signatures by the private keys
        internal EnvelopeContent content;

        // includes the timestamps over the data+signatures
        internal Postmarks postmarks;

        // For management overhead
        public Envelope myEnvelope { get; internal set; }

        public Payload()
        {
            content = new EnvelopeContent();
        }

        public Payload(Asn1Encodable asn1Encodable) : base(asn1Encodable)
        {
            Deserialize(asn1Encodable);
        }

        public override void Deserialize(Asn1Encodable asn1Encodable)
        {
            if (asn1Encodable == null)
                throw new ArgumentNullException("asn1Encodable");
            var sequence = Asn1Sequence.GetInstance(asn1Encodable);
            if (sequence == null)
                throw new ArgumentException("asn1Encodable", "Payload data is not a sequence");
            if (sequence.Count != 2)
                throw new ArgumentException("asn1Encodable", "Payload: sequence count " + sequence.Count + " is not 2");
            content = new EnvelopeContent(sequence[0]);
            postmarks = new Postmarks(sequence[1]);
            content.myEnvelope = this.myEnvelope;
        }

        internal void AddContent(byte[] input)
        {
            content.AddAssertion(new Assertion(new MemoryStream(input.Clone() as byte[])));
        }

        internal void AddAssertion(Assertion content)
        {
            this.content.AddAssertion(content);
        }

        public Stream Serialize()
        {
            // we need this at the top scope
            Stream outputstream = new MemoryStream();

            // 1 serialize the EnvelopeContent
            Stream streamEnvelopeContent = new MemoryStream();
            content.Serialize().CopyTo(streamEnvelopeContent);
            streamEnvelopeContent.Position = 0;

            // 2 generate the Postmarks
            postmarks = new Postmarks(streamEnvelopeContent);
            streamEnvelopeContent.Position = 0;

            // 3 create the sequence
            var dsg = new Org.BouncyCastle.Asn1.DerSequenceGenerator(outputstream);
            dsg.AddObject(content.ToAsn1Encodable());
            dsg.AddObject(postmarks.ToAsn1Encodable());
            dsg.Close();

            outputstream.Seek(0, SeekOrigin.Begin);
            return outputstream;
        }

        public Asn1Encodable toAsn1Encodable()
        {
            return Asn1Object.FromStream(Serialize());
        }

        internal void AddChop(Chop equipment)
        {
            content.AddChop(equipment);

        }

        internal byte[] ToByteArray()
        {
            var s = Serialize();
            var buf = new byte[s.Length];
            s.Read(buf, 0, buf.Length);
            return buf;
        }

        internal void AddRequiredProof(Assertion input)
        {
            this.content.AddRequiredProof(input);
        }
    }
}
