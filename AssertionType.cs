﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using Org.BouncyCastle.Asn1;

namespace ClericalStorage
{
    #region public AssertionType
    // Serialize() complete
    /// <summary>
    /// Identifies the OID or MIME-type of the Assertion
    /// </summary>
    /// <remarks>
    /// AssertionType ::= CHOICE {
    ///     publicKeyParamSet OBJECT IDENTIFIER,
    ///     mimeType UTF8String }
    /// </remarks>
    public class AssertionType : Deserializable
    {
        private static readonly Regex OidRegex = new Regex(@"\A([0-2])(\.([0-9]+))+\z");

        internal DerObjectIdentifier _oid;
        internal string mimeType;

        public override string ToString()
        {
            if (mimeType != null)
                return _oid.ToString();
            return mimeType;
        }

        /// <summary>
        /// Type of data included in Assertion
        /// </summary>
        /// <param name="type">MIME-type or OID string</param>
        public AssertionType(string type)
        {
            if (type == null)
                throw new ArgumentException("AssertionType: expected a string!");
            SetType(type);
        }

        public AssertionType(Asn1TaggedObject assertionT)
        {
            Deserialize(assertionT);
        }

        /// <summary>
        /// What kind of data is in the attached Assertion?
        /// </summary>
        /// <param name="mytype">Either a MIME-type or OID string</param>
        /// <remarks>
        /// There'outputStream no functional difference between OIDs and MIME-types.
        /// The only reason it'outputStream defined this way is because of the ASN.1
        /// CHOICE semantic.
        /// </remarks>
        public void SetType(string mytype = "application/octet-string")
        {
            // MIME-type will be more common, so first
            if (mytype.IndexOf('/') > -1)
            {
                // This appears to be a MIME-type
                this.mimeType = mytype;
                this._oid = null;
                return;
            } // If it's not a MIME-type, it must be an OID
            else if (!OidRegex.IsMatch(mytype))
            {
                // not a MIME-type, not an OID
                throw new ArgumentException("Must be MIME-type or OID string");
            }
            this.mimeType = null;
            _oid = new DerObjectIdentifier(mytype);
        }

        internal Stream Serialize()
        {
            Stream o = new MemoryStream();
            if (_oid != null)
            {
                var der = _oid;
                o.Write(der.GetDerEncoded(), 0, der.GetDerEncoded().Length);
            }
            else
            {
                var der = new DerUtf8String(this.mimeType);
                o.Write(der.GetDerEncoded(), 0, der.GetDerEncoded().Length);
            }
            o.Position = 0;
            return o;
        }

        internal Asn1Encodable ToAsn1Encodable()
        {
            return Asn1Object.FromStream(Serialize());
        }

        public override void Deserialize(Asn1Encodable value)
        {
            var taggedObject = value as Asn1TaggedObject;
            System.Diagnostics.Debug.Assert(taggedObject != null);

            if (taggedObject.TagNo != 7)
                {
                    throw new Org.BouncyCastle.Security.Certificates.CertificateEncodingException(
                        "AssertionType: tag " + taggedObject.TagNo + " is not 7");
                }
            
            DerUtf8String myString;
            DerObjectIdentifier myID;
            var assertion = taggedObject.GetObject();
            myString = DerUtf8String.GetInstance(assertion);
            if (myString != null) { SetType(myString.ToString()); return; }
            myID = DerObjectIdentifier.GetInstance(assertion);
            if (myID != null)
            {
                _oid = myID;
                mimeType = null;
                return;
            }
            throw new Org.BouncyCastle.Security.Certificates.
                CertificateException("AssertionType: " +
                assertion.ToString() + " neither MIME-type nor OID");
        }
    }
    #endregion
}
