﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Asn1;

namespace ClericalStorage
{
    /// <summary>
    /// The Container for Assertions and Commitments
    /// </summary>
    /// <remarks>
    /// ACContainer ::= SEQUENCE {
    ///     assertions Assertions,
    ///     commitments Commitments }
    /// </remarks>
    public class ACContainer : Deserializable
    {
        internal Assertions assertions { get; private set; }
        internal Commitments commitments { get; private set; }

        /// <summary>
        /// Initialize with the Assertions object
        /// </summary>
        /// <param name="input">object containing every Assertion</param>
        internal ACContainer()
        {
            assertions = new Assertions();
            commitments = new Commitments(assertions.assertionsOutputStream);
        }

        /// <summary>
        /// Constructor for deserialization and verification of inbound Envelope
        /// </summary>
        /// <param name="validityDataSequence">an Asn1Sequence containing the Assertions and Commitments</param>
        public ACContainer(Asn1Encodable asn1Encodable)
            : base(asn1Encodable)
        {
            Deserialize(asn1Encodable);
        }

        public override void Deserialize(Asn1Encodable input)
        {
            var sequence = Asn1Sequence.GetInstance(input);
            if (sequence == null)
                throw new ArgumentException("ACContainer: deserialize " + input + " is not an Asn1Sequence");
            if (sequence.Count != 2)
                throw new Org.BouncyCastle.Security.Certificates.
                    CertificateException("ACContainer sequence " + sequence.Count + " not 2");
            assertions = new Assertions(sequence[0]);
            commitments = new Commitments(sequence[1], sequence[0].GetDerEncoded());
        }

        [Obsolete("Use member 'assertions' instead")]
        internal Assertions GetAssertions()
        {
            return assertions;
        }

        internal Stream Serialize()
        {
            Stream output = new MemoryStream();

            Stream assertionserial = assertions.Serialize();
            commitments = new Commitments(assertionserial);
            Stream commitmentserial = commitments.Serialize();
            var s = new DerSequenceGenerator(output);
            s.AddObject(Asn1Object.FromStream(assertionserial));
            s.AddObject(Asn1Object.FromStream(commitmentserial));
            s.Close();
            output.Position = 0;
            return output;
        }

        Asn1Encodable ToAsn1Encodable()
        {
            return Asn1Object.FromStream(Serialize());
        }


        internal void AddAssertion(Assertion content)
        {
            assertions.Add(content);
        }

        internal byte[] ToByteArray()
        {
            var s = Serialize();
            var b = new byte[s.Length];
            s.Read(b, 0, b.Length);
            return b;
        }

        /// <summary>
        /// Necessary so that assertions.myEnvelope can have its required Assertions set.
        /// </summary>
        public Envelope myEnvelope
        {
            get { return assertions.myEnvelope; }
            internal set { assertions.myEnvelope = value; }
        }
    }
}
