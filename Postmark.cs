﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using asn1 = Org.BouncyCastle.Asn1;
using asn1tsp = Org.BouncyCastle.Asn1.Tsp;
using asn1cms = Org.BouncyCastle.Asn1.Cms;
using tsp = Org.BouncyCastle.Tsp;
using security=Org.BouncyCastle.Security;
using crypto = Org.BouncyCastle.Crypto;

namespace ClericalStorage
{
    #region public Postmark
    /// <summary>
    /// Timestamp over EnvelopeContent
    /// </summary>
    /// <remarks>
    /// Postmark ::= TimeStampToken
    /// 
    /// BouncyCastle has an odd TSP implementation, which prevents this
    /// from actually deriving therefrom.
    /// </remarks>
    public class Postmark
    {
        private string digestName;
        private crypto.IDigest myDigest;
        private System.IO.Stream streamToPostmark;
        private byte[] actualPostmarkData;
        private Org.BouncyCastle.Tsp.TimeStampToken asToken;


        public Postmark(string digestName, System.IO.Stream streamToPostmark)
        {
            this.digestName = digestName;
            this.myDigest = security.DigestUtilities.GetDigest(digestName);
            this.streamToPostmark = streamToPostmark;
        }

        /// <summary>
        /// Obtains the actual TimeStampToken over the stream.
        /// </summary>
        /// <returns>Stream containing the DER of the TimeStampToken</returns>
        /// <remarks>
        /// Recalculates the digest every time it's called.
        /// </remarks>
        public System.IO.Stream Serialize()
        {
            streamToPostmark.Position = 0;

            // Create the Time Stamp Request
                // First, digest the streamToPostmark
            var toPostmark = new byte[streamToPostmark.Length];
            streamToPostmark.Read(toPostmark, 0, toPostmark.Length);
            myDigest.BlockUpdate(toPostmark, 0, toPostmark.Length);
            var actualDigestBytes = new byte[myDigest.GetDigestSize()];
            myDigest.DoFinal(actualDigestBytes, 0);
                // Next, generate the time stamp request
            var tsrg = new Org.BouncyCastle.Tsp.TimeStampRequestGenerator();
            var tsreq = tsrg.Generate(Org.BouncyCastle.Security.DigestUtilities.GetObjectIdentifier(digestName).Id, actualDigestBytes);


            //string[] rfc3161serviceURLs = new string[] {
            //    "http://www.startssl.com/timestamp",
            //    "http://timestamp.comodoca.com/rfc3161"
            //};

            
                // Then submit it for timestamping
            var rfc3161serviceURL = "http://timestamp.comodoca.com/rfc3161";
            var webRequest = System.Net.WebRequest.Create(rfc3161serviceURL) as System.Net.HttpWebRequest;

            webRequest.Accept = "application/timestamp-reply";
            webRequest.AllowWriteStreamBuffering = true;
            webRequest.ContentType = "application/timestamp-query";
            webRequest.Method = "POST";
            webRequest.AllowAutoRedirect = false;
            webRequest.AutomaticDecompression = System.Net.DecompressionMethods.None;
            webRequest.Pipelined = false;
            webRequest.SendChunked = false;
            webRequest.UserAgent = "Envelope/0.1.0";

            var webRequestPayload = tsreq.GetEncoded();
            // webRequest.ContentLength = d.Length;
            
            var requestStream = webRequest.GetRequestStream();
            requestStream.Write(webRequestPayload, 0, (int)webRequestPayload.Length);

            // Here is where we get the response
            var e = webRequest.GetResponse();
            if (e.ContentType != "application/timestamp-reply")
                throw new IOException("cannot comprehend the response for the timestamp request");
            var f = e.GetResponseStream();

            MemoryStream h = new MemoryStream();
            f.CopyTo(h);
            h.Position = 0;
            var g = new byte[h.Length];
            h.Read(g, 0, g.Length);
            // let's explore this response a bit
            var response = new Org.BouncyCastle.Tsp.TimeStampResponse(g);
            if (response.Status != 0)
            {
                PlatformInterface.nullTimeStampResponses.Add(response);
                return null;
            }
            PlatformInterface.TimeStampTokens.Add(response.TimeStampToken);
            actualPostmarkData = response.TimeStampToken.GetEncoded();

            var ms = new System.IO.MemoryStream(actualPostmarkData);
            ms.Position = 0;
            return ms;
        }

        public byte[] AsByteArray()
        {
            System.IO.Stream myinput = Serialize();
            byte[] buf = new byte[myinput.Length];
            myinput.Read(buf, 0, buf.Length);
            return buf;
        }

        public asn1.Asn1Encodable toAsn1Encodable()
        {
            var mytemp = Serialize();
            if (mytemp == null)
                return null;
            return Org.BouncyCastle.Asn1.Asn1Object.FromStream(mytemp);
        }

        internal static Postmark GetInstance(object sequenceEntry)
        {
            var j = asn1cms.ContentInfo.GetInstance(sequenceEntry);
            if (j == null)
                throw new ArgumentException("sequenceEntry", "Postmark: GetInstance didn't receive a ContentInfo");
            return new Postmark(j);
        }

        internal Postmark(Org.BouncyCastle.Asn1.Cms.ContentInfo input)
        {
            Deserialize(input);
        }

        protected /*override*/ void Deserialize(Org.BouncyCastle.Asn1.Asn1Encodable input)
        {
            actualPostmarkData = input.GetEncoded();
            asToken = new tsp.TimeStampToken(Org.BouncyCastle.Asn1.Cms.ContentInfo.GetInstance(input));
        }
    }
    #endregion
}
