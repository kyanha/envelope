﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Asn1;

namespace ClericalStorage
{
    /// <remarks>
    /// CopyrightLicense ::= SEQUENCE(0..MAX) OF LicenseClause
    /// LicenseClause ::= SEQUENCE {
    ///     parsableCopyrightTerm OBJECT IDENTIFIER ,
    ///     languageOfControl UTF8String,
    ///     legibleCopyrightText LegibleClauses OPTIONAL }
    /// LegibleClauses ::= SEQUENCE(0..MAX) OF LegibleClause
    /// LegibleClause ::= SEQUENCE {
    ///     languageCode UTF8String,
    ///     textOfLicense UTF8String }
    /// </remarks>
    public class LegibleClauses : Deserializable
    {
        /// <summary>
        /// The Set of LegibleClause associated with this LicenseClause
        /// </summary>
        public ISet<LegibleClause> data;

        public LegibleClauses()
        {
            data = new HashSet<LegibleClause>();
        }

        public LegibleClauses(Asn1Encodable legibleClauseSequence)
        {
            Deserialize(legibleClauseSequence);
        }

        public System.Collections.IEnumerator GetEnumerator()
        {
            return data.GetEnumerator();
        }

        public void Add(LegibleClause value)
        {
            data.Add(value);
        }

        internal Stream Serialize()
        {
            MemoryStream s = new MemoryStream();
            var dsg = new DerSequenceGenerator(s);
            foreach (LegibleClause i in data)
            {
                dsg.AddObject(i.ToAsn1Encodable());
            }
            dsg.Close();
            s.Position = 0;
            return s;
        }

        internal Asn1Encodable ToAsn1Encodable()
        {
            return Asn1Object.FromStream(Serialize());
        }

        internal byte[] AsByteArray()
        {
            var s = Serialize();
            byte[] buf = new byte[s.Length];
            s.Read(buf, 0, buf.Length);
            return buf;
        }


        public override void Deserialize(Asn1Encodable legibleClauseSequence)
        {
            if (null == legibleClauseSequence)
                throw new ArgumentNullException("legibleClauseSequence");
            var sequence = Org.BouncyCastle.Asn1.Asn1Sequence.GetInstance(legibleClauseSequence);
            if (null == sequence)
                throw new ArgumentException("LegibleClauses: Expected a Sequence", "legibleClauseSequence");
            foreach (var i in sequence)
            {
                var innerSequence = Org.BouncyCastle.Asn1.Asn1Sequence.GetInstance(i);
                if (innerSequence == null)
                    throw new ArgumentException("Expected Sequence of sequences, sequence contained " + i.ToString() + " instead");
                if (innerSequence.Count != 2)
                    throw new ArgumentException("Inner sequence expected to contain 2 members, contains " + innerSequence.Count);
                var is0 = Org.BouncyCastle.Asn1.DerUtf8String.GetInstance(innerSequence[0]);
                var is1 = Org.BouncyCastle.Asn1.DerUtf8String.GetInstance(innerSequence[1]);
                data.Add(new LegibleClause(is0.GetString(), is1.GetString()));
            }
        }
    }
}
