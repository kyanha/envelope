﻿using Org.BouncyCastle.Asn1;

namespace ClericalStorage
{
    /// <summary>
    /// Container for Stamp
    /// </summary>
    /// <remarks>
    /// -- The Stamps are just Assertions that are outside the Payload and unencrypted.
    /// ( Stamps ::= SEQUENCE(0..MAX) OF Stamp, Stamp ::= Assertion )
    /// ...
    /// Stamps ::= Assertions
    /// </remarks>
    public class Stamps : Assertions
    {
        public Stamps(byte[] value)
            : base(Asn1Object.FromByteArray(value))
        {
            Deserialize(Asn1Object.FromByteArray(value));
        }

        public Stamps()
            : base()
        {
        }
    }
}
