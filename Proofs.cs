﻿using System;
using System.IO;
using System.Collections.Generic;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Security;

namespace ClericalStorage
{
    #region public Proofs
    /// <summary>
    /// Sequence of individual Proof
    /// </summary>
    /// <remarks>-- Signatures by all of the end-entity keys asserted
    /// Proofs ::= SEQUENCE(1..MAX) OF Proof
    /// </remarks>
    public class Proofs : Deserializable
    {
        public List<Proof> data = new List<Proof>();
        public List<Proof> verification = new List<Proof>();
        public List<Chop> chops = new List<Chop>();

        private ACContainer acObject;
        private byte[] acContainerDerEncoded;
        private Stream ACdata;

        private IList<Assertion> requiredProofs = new List<Assertion>();

        public Proofs()
        {
            // TODO: Complete member initialization
            // cannot rely on having ACder when this object is constructed,
            // Payload.AddChop calls EnvelopeContent.AddChop calls Proofs.AddChop
        }

        public Proofs(Asn1Encodable asn1Encodable) :base(asn1Encodable)
        {
            Deserialize(asn1Encodable);
        }

        public Proofs(ACContainer envelopeContent)
        {
            acObject = envelopeContent;
        }

        public IList<Assertion> proofsNeeded = new List<Assertion>();

        /// <summary>
        /// Verifies that all of the inner Proofs are legitimate
        /// </summary>
        /// <returns>true, or false if a signature fails and Config.Configuration.throwOnFailedVerify is false</returns>
        /// <remarks>This needs to try to look up the revocation information for the required Proofs.</remarks>
        // FIXME: get revocation information for all requiredProofs
        // FIXME: For every Proof, set failure for any proof that initially verifies but is unverified
        
        public bool Verify()
        {
            bool verified = true;
            foreach (var i in data)
            {
                if (!i.Verify())
                    if (Config.Configuration.throwOnFailedVerify)
                        throw new Org.BouncyCastle.Security.SignatureException("Proofs: Verify failed on " + i);
                    verified = false;
            }
            return verified;
        }

        public override void Deserialize(Asn1Encodable asn1Encodable)
        {
            var derSequence = DerSequence.GetInstance(asn1Encodable);
            if (derSequence == null)
                throw new Org.BouncyCastle.Security.Certificates.CertificateParsingException(
                    "Proofs: expected a sequence");
            foreach (var proof in derSequence)
            {
                var newProof = new Proof(proof as Asn1Encodable);
                newProof.SetStreamToDigest(this.ACdata);
            }
        }

        public void AddChop(Chop input)
        {
            chops.Add(input);
            foreach (var i in data)
            {
                if (i != null)
                    i.EquipChop(input);
            }
        }

        public Stream Serialize()
        {
            System.Diagnostics.Debug.Assert(chops.Count > 0);
            ISigner mySigner;

            foreach (var chop in chops)
            {
                foreach (var digestName in Config.Configuration.ecChopProofDigestAlgorithms)
                {
                    if (digestName.StartsWith("SHA"))
                    {
                        mySigner = new Org.BouncyCastle.Crypto.Signers.DsaDigestSigner(
                            new Org.BouncyCastle.Crypto.Signers.ECDsaSigner(), DigestUtilities.GetDigest(digestName));
                    }
                    else if (digestName.StartsWith("GOST"))
                    {
                        mySigner = new Org.BouncyCastle.Crypto.Signers.DsaDigestSigner(
                            new Org.BouncyCastle.Crypto.Signers.ECGost3410Signer(), DigestUtilities.GetDigest(digestName));
                    }  else {
                        mySigner=new Org.BouncyCastle.Crypto.Signers.DsaDigestSigner(
                            new Org.BouncyCastle.Crypto.Signers.ECNRSigner(), DigestUtilities.GetDigest(digestName));
                    }

                    data.Add(new Proof(mySigner, new MemoryStream(acContainerDerEncoded), chop));
                }
            }
            ACdata = new MemoryStream();
            var dsg = new DerSequenceGenerator(ACdata);
            foreach (var i in data)
                dsg.AddObject(i.ToAsn1Encodable());
            dsg.Close();
            ACdata.Position = 0;
            return ACdata;
        }

        public Asn1Encodable ToAsn1Encodable()
        {
            return Asn1Object.FromStream(Serialize());
        }

        public byte[] ToByteArray()
        {
            var s = Serialize();
            var b = new byte[s.Length];
            s.Read(b, 0, b.Length);
            return b;
        }

        public void SetACContent(byte[] ACder)
        {
            this.acContainerDerEncoded = ACder;
        }

        internal void AddRequiredProof(Assertion input)
        {
            proofsNeeded.Add(input);
        }
    }
    #endregion
}