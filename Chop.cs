﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Security.Certificates;
using Org.BouncyCastle.X509;

namespace ClericalStorage
{
    /// <summary>
    /// A signature device and/or verifier
    /// </summary>
    /// <remarks>
    /// Contains public key
    /// May contain private key
    /// Does not need to implement Deserialize()
    /// May accept an X509Certificate
    /// </remarks>
    public class Chop
    {
        public Org.BouncyCastle.Crypto.AsymmetricKeyParameter publicKey
        {
            get;
            protected set;
        }
        public Org.BouncyCastle.Crypto.AsymmetricKeyParameter privateKey { get; protected set; }
        /// <summary>
        /// 
        /// </summary>
        public byte[] keyIdentifier
        {
            get
            {
                byte[] output = new byte[Config.Configuration.keyIdentifierDigest.GetByteLength()];
                Config.Configuration.keyIdentifierDigest.DoFinal(output, 0);
                return output;
            }
        }
        public Org.BouncyCastle.Crypto.AsymmetricCipherKeyPair identityChop
        {
            get {
                return new AsymmetricCipherKeyPair(publicKey,privateKey);
            }
            set
            {
                publicKey = value.Public;
                privateKey = value.Private;
            }
        }
        public bool verifyOnly { get { return (privateKey == null); } }
        public X509Certificate whoAmI = null;

        internal Chop(Org.BouncyCastle.Crypto.AsymmetricCipherKeyPair keypair)
        {
            identityChop = keypair;

        }

        public Chop(AsymmetricKeyParameter parameter)
        {
            if (parameter == null)
                throw new ArgumentNullException("publicKey");
			if (!parameter.IsPrivate)
				this.publicKey = parameter;
			else
			{
				this.privateKey = parameter;
				
			}
        }

        protected Chop(X509Certificate input)
        {
            whoAmI = input;
            publicKey = input.GetPublicKey();
        }

        internal static Chop GetNew()
        {
            Org.BouncyCastle.Crypto.AsymmetricCipherKeyPair keypair;
            {
                var eckpg = new Org.BouncyCastle.Crypto.Generators.ECKeyPairGenerator("EC");
                var genparm = new Org.BouncyCastle.Crypto.KeyGenerationParameters(
                    new Org.BouncyCastle.Security.SecureRandom(
                        new Org.BouncyCastle.Crypto.Prng.CryptoApiRandomGenerator()), 256);
                eckpg.Init(genparm);
                keypair = eckpg.GenerateKeyPair();
            }
            return new Chop(keypair);
        }
    }
}
