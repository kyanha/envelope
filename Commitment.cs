﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Asn1;
using System.IO;
using Signers = Org.BouncyCastle.Crypto.Signers;
using Crypto = Org.BouncyCastle.Crypto;
using ClericalStorage.Config;

namespace ClericalStorage
{
    /// <summary>
    /// Single digest value over Assertions
    /// </summary>
    /// <remarks>
    /// Commitment ::= SEQUENCE {
    ///     hashAlgorithm AlgorithmIdentifier,
    ///     hashValue OCTET STRING }
    /// </remarks>
    public class Commitment : Deserializable
    {
        #region Public Members
        // This can be either an OID or a BouncyCastle digest name string
        public string hashAlgorithm { get; private set; }
        public DerObjectIdentifier hashAlgorithmOID { get; private set; }
        public byte[] nomineeDigestValue { get; private set; }
        #endregion

        #region Private members
        // Holds a reference to the stream
        // Note: not evaluated until Serialize() or Verify()
        private Stream assertionsStream;

        // The digest implementation
        private Org.BouncyCastle.Crypto.IDigest myDigest;
        #endregion

        public Commitment(string algID,
            System.IO.Stream toHash)
        {
            if (algID == null)
                throw new ArgumentNullException("algID");
            if (toHash == null)
                assertionsStream = new MemoryStream();
            this.assertionsStream = toHash;
            myDigest = Org.BouncyCastle.Security.DigestUtilities.GetDigest(algID);
            hashAlgorithmOID = Org.BouncyCastle.Security.DigestUtilities.GetObjectIdentifier(algID);
            hashAlgorithm = Org.BouncyCastle.Security.DigestUtilities.GetAlgorithmName(hashAlgorithmOID);
        }

        public Commitment(Org.BouncyCastle.Asn1.DerObjectIdentifier algID,
            byte[] contentToHash):base()
        {
            if (algID == null)
                throw new ArgumentNullException("algID");
            System.Diagnostics.Debug.Assert(contentToHash != null);
            if (contentToHash == null)
                assertionsStream = new MemoryStream();
            else
                assertionsStream = new MemoryStream(contentToHash);
            myDigest = Org.BouncyCastle.Security.DigestUtilities.GetDigest(algID);
            hashAlgorithm = Org.BouncyCastle.Security.DigestUtilities.GetAlgorithmName(Org.BouncyCastle.Security.DigestUtilities.GetObjectIdentifier(algID.ToString()));
        }

        public Commitment(Org.BouncyCastle.Asn1.DerObjectIdentifier algID,
            System.IO.Stream toHash) :base()
        {
            if (algID == null)
                throw new ArgumentNullException("algID");
            System.Diagnostics.Debug.Assert(toHash != null);
            if (toHash == null)
                this.assertionsStream = new MemoryStream();
            else
                this.assertionsStream = toHash;
            hashAlgorithmOID = Org.BouncyCastle.Security.DigestUtilities.GetObjectIdentifier(algID.ToString());
            myDigest = Org.BouncyCastle.Security.DigestUtilities.GetDigest(algID);
            hashAlgorithm = Org.BouncyCastle.Security.DigestUtilities.GetAlgorithmName(hashAlgorithmOID);
        }

        public Commitment(Asn1Encodable value)
            : base(value)
        {
            // This is the deserialization constructor
            var m = DerSequence.GetInstance(value);
            if (m == null || m.Count != 2)
                throw new Org.BouncyCastle.Security.Certificates.CertificateEncodingException(
                    "Commitment: " +
                    m == null ? "null" : m.Count.ToString() +
                    " is not 2 (sequence count)");
            hashAlgorithmOID = DerObjectIdentifier.GetInstance(m[0]);
            if (hashAlgorithmOID == null)
                throw new Org.BouncyCastle.Security.Certificates.CertificateEncodingException(
                    "Commitment: " + m[0] + " not an algorithm OID");
            var derOctetString = DerOctetString.GetInstance(m[1]);
            if (derOctetString == null)
                throw new Org.BouncyCastle.Security.Certificates.CertificateEncodingException(
                    "Commitment: " + m[1] + " not an octet string");
            nomineeDigestValue = derOctetString.GetOctets();

            myDigest = Org.BouncyCastle.Security.DigestUtilities.GetDigest(hashAlgorithmOID);
            hashAlgorithm = Org.BouncyCastle.Security.DigestUtilities.GetAlgorithmName(hashAlgorithmOID);

            // Next step: ensure nomineeDigestValue matches streamToBeSerialized in Verify()
        }

        internal bool Verify()
        {
            // if we don't have a nomineeDigestValue, we can't verify
            if (nomineeDigestValue == null)
                throw new InvalidOperationException("Commitment.Verify: no nomineeDigestValue");
            // if streamToHash isn't set, there's nothing to verify
            if (assertionsStream == null)
                throw new InvalidOperationException("Commitment.Verify: streamToHash unset");
            assertionsStream.Position = 0;

            byte[] myArrayToHash = new byte[assertionsStream.Length];
            myDigest.Reset();
            assertionsStream.Read(myArrayToHash, 0, myArrayToHash.Length);
            myDigest.BlockUpdate(myArrayToHash, 0, myArrayToHash.Length);

            byte[] outbytes = new byte[myDigest.GetDigestSize()];
            myDigest.DoFinal(outbytes, 0);
            bool verification = true;
            // constant-time digest comparison
            for (int i = 0; i < outbytes.Length; i++)
            {
                if (nomineeDigestValue[i] != outbytes[i])
                    verification = false;
            }
            if(Configuration.throwOnFailedVerify)
                throw new Org.BouncyCastle.Security.GeneralSecurityException("Commitment " + this.ToString() + "failed Verify()");
            return verification;

        }

        internal System.IO.Stream Serialize()
        {

            // verify assumptions
            Debug.Assert(this.nomineeDigestValue == null);
            Debug.Assert(this.assertionsStream != null);
            Debug.Assert(this.assertionsStream.Length > 0);
            Debug.Assert(this.myDigest != null);

            Stream output = new MemoryStream();

            myDigest.Reset();

            byte[] hashtemp = new byte[assertionsStream.Length];
            assertionsStream.Read(hashtemp, 0, hashtemp.Length);
            myDigest.BlockUpdate(hashtemp, 0, hashtemp.Length);

            byte[] outbytes = new byte[myDigest.GetDigestSize()];
            myDigest.DoFinal(outbytes, 0);

            // Now that we've got the hash, we need to present ourselves as a Sequence

            var seqgen = new Org.BouncyCastle.Asn1.DerSequenceGenerator(output);
            seqgen.AddObject(Org.BouncyCastle.Security.DigestUtilities.GetObjectIdentifier(hashAlgorithm));
            seqgen.AddObject(new Org.BouncyCastle.Asn1.DerOctetString(outbytes));
            seqgen.Close();
            output.Position = 0;
            return output;
        }

        internal byte[] AsByteArray()
        {
            Stream s = Serialize();
            byte[] buf = new byte[s.Length];
            s.Read(buf, 0, buf.Length);
            return buf;
        }

        internal Asn1Encodable ToAsn1Encodable()
        {
            return Asn1Object.FromStream(Serialize());
        }

        public override void Deserialize(Asn1Encodable value)
        {
            throw new NotImplementedException();
        }
    }
}
