﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Asn1;
using System.IO;

namespace ClericalStorage
{
    // TODO: Completely redo copyright clauses
    /// <summary>
    /// Container for CopyrightLicense info (self-contained)
    /// </summary>
    /// <remarks>
    /// CopyrightLicense ::= SEQUENCE(0..MAX) OF LicenseClause
    /// LicenseClause ::= SEQUENCE {
    ///     parsableCopyrightTerm OBJECT IDENTIFIER ,
    ///     languageOfControl UTF8String,
    ///     legibleCopyrightText LegibleClauses OPTIONAL }
    /// LegibleClauses ::= SEQUENCE(0..MAX) OF LegibleClause
    /// LegibleClause ::= SEQUENCE {
    ///     languageCode UTF8String,
    ///     textOfLicense UTF8String }
    /// </remarks>

    public class CopyrightLicense:Deserializable
    {
        #region Private members
        private List<LicenseClause> licenseClauses = new List<LicenseClause>();
        #endregion
        #region Constructor
        public CopyrightLicense()
        {
            // All Rights Reserved object
            licenseClauses.Add(new LicenseClause("1.3.6.1.4.1.22232.7.1.3.0"));
        }

        public CopyrightLicense(byte[] value)
        {
            // Top level object, value is a DerOctetString encapsulating a Sequence
            Deserialize(Asn1Object.FromByteArray(value));
        }
        #endregion
        #region Add a new license clause using one of three parameters
        public void Add(LicenseClause c)
        {
            licenseClauses.Add(c);
        }
        public void Add(DerObjectIdentifier c)
        {
            licenseClauses.Add(new LicenseClause(c.ToString()));
        }
        /// <summary>
        /// Add a new license clause using the string representation of an OID
        /// </summary>
        /// <param name="c">String in dotted OID form: [0-2](\.[0-9]+)+</param>
        public void Add(string c)
        {
            licenseClauses.Add(new LicenseClause(c));
        }
        #endregion
        #region Serialize and ToAsn1Encodable
        internal Stream Serialize()
        {
            MemoryStream s = new MemoryStream();
            var dsg = new DerSequenceGenerator(s);
            foreach (var i in licenseClauses)
            {
                dsg.AddObject(i.ToAsn1Encodable());
            }
            dsg.Close();
            s.Position = 0;
            return s;
        }

        internal Asn1Encodable ToAsn1Encodable()
        {
            return Asn1Object.FromStream(Serialize());
        }
        #endregion

        public bool Valid { get; set; }

        public override void Deserialize(Asn1Encodable value)
        {
            // As a top-level object, we are passed an Octet String
            // encapsulating the sequence of LicenseClause.
            var A = Asn1Sequence.GetInstance(Asn1OctetString.GetInstance(value));
                if (null == A) throw new ArgumentException("CopyrightLicense: value must be a Sequence");
            foreach (Asn1Encodable B in A)
            {
                licenseClauses.Add(new LicenseClause(B));
            }
        }
    }
}
