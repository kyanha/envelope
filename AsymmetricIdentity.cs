﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.X509;

namespace ClericalStorage
{
    /// <summary>
    /// Public key for an intended recipient (and associated metadata)
    /// </summary>
    /// <remarks>
    /// </remarks>
    public class AsymmetricIdentity
    {
        /// <summary>
        /// The public key for this recipient.
        /// </summary>
        public Org.BouncyCastle.Crypto.AsymmetricKeyParameter publicKey;

        /// <summary>
        /// Encrypts the byte[] to this recipient, using myKey if necessary for key agreement
        /// </summary>
        /// <param name="data">The data to send (usually a symmetric key)</param>
        /// <param name="myKey">The private key being used to derive the agreement</param>
        /// <returns>data, AES192-encrypted with the derived key</returns>
        /// <remarks>
        /// SHA384, AES192, EC.  the function the AES key is derived with
        /// (key agreement is ECDH, then it'outputStream run through a derivation function)
        /// is Kdf2BytesGenerator/SHA384.
        /// </remarks>
        public byte[] SendTo(byte[] data, Chop myKey)
        {
            if (this.publicKey != null && this.publicKey is Org.BouncyCastle.Crypto.Parameters.ECPublicKeyParameters && myKey == null)
                throw new InvalidOperationException("need a Chop to send to a recipient");
            if (data == null)
                throw new NullReferenceException("data");
            if (data.Length == 0)
                return data; // no use doing nothing

            var ECDestinationKey = this.publicKey as Org.BouncyCastle.Crypto.Parameters.ECPublicKeyParameters;
            var RSADestinationKey = this.publicKey as Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters;
            var DHDestinationKey = this.publicKey as Org.BouncyCastle.Crypto.Parameters.DHPublicKeyParameters;
            var ElGamalDestinationKey = this.publicKey as Org.BouncyCastle.Crypto.Parameters.ElGamalPublicKeyParameters;
            var lmnop = this.publicKey as Org.BouncyCastle.Crypto.Parameters.Gost3410PublicKeyParameters;
            var egpkp = this.publicKey as Org.BouncyCastle.Crypto.Parameters.ElGamalPublicKeyParameters;
            if (this.publicKey is Org.BouncyCastle.Crypto.Parameters.DsaPublicKeyParameters)
                throw new InvalidOperationException("Cannot send to a DSA public key");

            // If the destination key is an RSA public key, we don't have any problems.
            if (RSADestinationKey != null)
            {
                var a1 = new Org.BouncyCastle.Crypto.Engines.RsaEngine();
                a1.Init(true, RSADestinationKey);
                var a3 = new Org.BouncyCastle.Crypto.Paddings.Pkcs7Padding();
                a3.AddPadding(data,0);
                var a2 = a1.ProcessBlock(data, 0, data.Length);
                return a2;
            }
            if (ECDestinationKey != null)
            {
                if (ECDestinationKey.Parameters != ((Org.BouncyCastle.Crypto.Parameters.ECPublicKeyParameters)myKey.publicKey).Parameters)
                    throw new InvalidOperationException("If destination is an EC public key, we can only send to a key on the same elliptical curve as our own");
                var s = new Org.BouncyCastle.Crypto.Agreement.ECDHWithKdfBasicAgreement("SHA384",
                    new Org.BouncyCastle.Crypto.Generators.Kdf2BytesGenerator(
                        new Org.BouncyCastle.Crypto.Digests.Sha384Digest()));
                s.Init(myKey.privateKey);
                var i = s.CalculateAgreement(publicKey).ToByteArray();
                // Now, use AES192 to encrypt using i as keying material
                var aes192 = new Org.BouncyCastle.Crypto.Engines.AesEngine();
                var blockcipher = new Org.BouncyCastle.Crypto.Modes.OfbBlockCipher(aes192, aes192.GetBlockSize());
                blockcipher.Init(true, new Org.BouncyCastle.Crypto.Parameters.KeyParameter(i));
                int r = data.Length / blockcipher.GetBlockSize() + (data.Length % blockcipher.GetBlockSize() > 0 ? 1 : 0);
                var t = new byte[r * blockcipher.GetBlockSize()];
                blockcipher.ProcessBlock(data, 0, t, 0);
                return t;
            }

            // If we've gotten here, I don't know how to handle it
            throw new InvalidOperationException("SendTo(): unknown destination key type");
        }

        /// <summary>
        /// Gets the parameter as DER-encoded SubjectPublicKeyInfo
        /// </summary>
        /// <returns>DER-encoded SubjectPublicKeyInfo of parameter</returns>
        public byte[] AsByteArray()
        {
            return SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo(publicKey).GetDerEncoded();
        }

        /// <summary>
        /// Obtain SubjectPublicKeyInfo of parameter as Asn1Encodable
        /// </summary>
        public Asn1Encodable ToAsn1Encodable()
        {
            return SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo(publicKey);
        }

        public AsymmetricIdentity(Org.BouncyCastle.Crypto.AsymmetricKeyParameter publicKey)
        {
            if (publicKey.IsPrivate)
                throw new ArgumentException("AsymmetricIdentity.ctor: expected a public key");
            this.publicKey = publicKey;
        }
    }
}
