﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Org.BouncyCastle.Asn1;

namespace ClericalStorage
{
    /// <summary>
    /// Individual statement { "en/US", "Creative Commons Attribution"}
    /// </summary>
    /// <remarks>
    /// CopyrightLicense ::= SEQUENCE(0..MAX) OF LicenseClause
    /// LicenseClause ::= SEQUENCE {
    ///     parsableCopyrightTerm OBJECT IDENTIFIER ,
    ///     languageOfControl UTF8String,
    ///     legibleCopyrightText LegibleClauses OPTIONAL }
    /// LegibleClauses ::= SEQUENCE(0..MAX) OF LegibleClause
    /// LegibleClause ::= SEQUENCE {
    ///     languageCode UTF8String,
    ///     textOfLicense UTF8String }
    /// </remarks>

    public class LegibleClause : Deserializable
    {
        #region public members
        public string languageCode { get; protected set; }
        public string textOfLicense { get; protected set; }

        // 
        public IDictionary<string, LegibleClause> data = new Dictionary<string, LegibleClause>();
        #endregion
        #region Constructors
        
        public LegibleClause(string languageCode, string textOfLicense)
        {
            if (null == languageCode)
                throw new ArgumentNullException("languageCode");
            if (null == textOfLicense)
                throw new ArgumentNullException("textOfLicense");
            this.languageCode = languageCode;
            this.textOfLicense = textOfLicense;
        }

        public LegibleClause(Asn1Encodable value) :base(value)
        {
            Deserialize(value);
        }

        public override void Deserialize(Asn1Encodable value)
        {
            var sequenceToDecode = Asn1Sequence.GetInstance(value);
            if (sequenceToDecode == null)
                throw new ArgumentException("LegibleClause: expected a sequence");
            if (sequenceToDecode.Count != 2)
                throw new ArgumentException("LegibleClause: sequence length " + sequenceToDecode.Count + " != 2");
            var String1 = DerUtf8String.GetInstance(sequenceToDecode[0]);
            if (String1 == null)
                throw new ArgumentException("LegibleClause: sequence[0] not DerUtf8String");
            languageCode = String1.GetString();
            var String2 = DerUtf8String.GetInstance(sequenceToDecode[1]);
            if (String2 == null)
                throw new ArgumentException("LegibleClause: sequence[1] not DerUtf8String");
            textOfLicense = String2.GetString();
        }

        #endregion


        internal Stream Serialize()
        {
            MemoryStream s = new MemoryStream();
            var dsg = new DerSequenceGenerator(s);
            dsg.AddObject(new DerUtf8String(languageCode));
            dsg.AddObject(new DerUtf8String(textOfLicense));
            dsg.Close();
            s.Position = 0;
            return s;
        }

        internal Asn1Encodable ToAsn1Encodable()
        {
            return Asn1Object.FromStream(Serialize());
        }

        internal byte[] AsByteArray()
        {
            var s = Serialize();
            byte[] buf = new byte[s.Length];
            s.Read(buf, 0, buf.Length);
            return buf;
        }
    }
}
