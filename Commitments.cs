﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Security;
using ClericalStorage.Config;

namespace ClericalStorage
{
    #region public Commitments
    /// <summary>
    /// Digest values over Assertions
    /// </summary>
    /// <remarks>
    /// -- Multiple hashing algorithms to future-proof in case of one or several
    /// -- compromise
    /// Commitments ::= SEQUENCE(1..MAX) OF Commitment
    /// </remarks>
    public class Commitments : Deserializable
    {
        List<Commitment> everyCommitment = new List<Commitment>();
        private Stream assertions;
        private byte[] AssertionsOctetVector;
        private bool readyForVerification = false;

        public Commitments(Stream assertions)
            : base()
        {
            DistributeCommitments(assertions);
        }

        private void DistributeCommitments(Stream assertions)
        {
            this.assertions = assertions;
            // GOST R3411
            everyCommitment.Add(new Commitment(Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers.GostR3411, assertions));
            // RIPE MD256
            everyCommitment.Add(new Commitment(Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers.RipeMD256, assertions));
            // MD5 (here for illustrative purposes)
            everyCommitment.Add(new Commitment(Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers.MD5, assertions));
            // MD2 (also here for illustrative purposes)
            everyCommitment.Add(new Commitment(Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers.MD2, assertions));
            // SHA-1
            everyCommitment.Add(new Commitment(Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers.IdSha1, assertions));
            // SHA-2 family
            everyCommitment.Add(new Commitment(Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers.IdSha384, assertions));
        }

        /// <summary>
        /// Deserialize a Commitments structure, setting it up for verification
        /// </summary>
        /// <param name="inboundEncodable">a DerSequence</param>
        /// <param name="assertions">Assertions object</param>
        public Commitments(Asn1Encodable inboundEncodable, byte[] assertionsOctetString)
            : base(inboundEncodable)
        {
            Deserialize(inboundEncodable);
            if (assertionsOctetString != null)
            {
                AssertionsOctetVector = assertionsOctetString;
                assertions = new MemoryStream();
                assertions.Write(assertionsOctetString, 0, assertionsOctetString.Length);
                assertions.Position = 0;
                readyForVerification = true;
            }
        }

        public void SetAssertionsOctetString(byte[] assertionsOctetString)
        {
            if (assertionsOctetString == null)
                throw new ArgumentNullException("assertionsOctetString");
            AssertionsOctetVector = assertionsOctetString;
            assertions = new MemoryStream();
            assertions.Write(assertionsOctetString, 0, assertionsOctetString.Length);
            assertions.Position = 0;
            readyForVerification = true;
        }

        public void Add(Commitment c)
        {
            everyCommitment.Add(c);
        }

        public Stream Serialize()
        {
            MemoryStream s = new MemoryStream();
            var dsg = new DerSequenceGenerator(s);
            foreach (var i in everyCommitment)
            {
                dsg.AddObject(i.ToAsn1Encodable());
            }
            dsg.Close();
            s.Position = 0;
            AssertionsOctetVector = new byte[s.Length];
            s.Read(AssertionsOctetVector, 0, AssertionsOctetVector.Length);
            readyForVerification = true;
            s.Position = 0;
            return s;
        }

        public Asn1Encodable ToAsn1Encodable()
        {
            return Asn1Object.FromStream(Serialize());
        }

        public byte[] AsByteArray()
        {
            Serialize();
            return AssertionsOctetVector;
        }

        public override void Deserialize(Asn1Encodable value)
        {
            // This is the constructor from a pre-existing ASN.1 object.
            // It requires breaking apart the sequence into its components.
            var inboundSequence = DerSequence.GetInstance(value);
            if (inboundSequence == null)
                throw new Org.BouncyCastle.Security.Certificates.CertificateEncodingException(
                "Commitments: " + value.ToString() + " not a sequence");
            if (inboundSequence.Count < 1)
                throw new Org.BouncyCastle.Security.Certificates.CertificateEncodingException(
                    "Commitments: sequence has " + inboundSequence.Count + " elements (< 1)");
            foreach (Asn1Encodable i in inboundSequence)
                everyCommitment.Add(new Commitment(i));
        }

        #region public Verify()

        public bool Verify()
        {
            bool returnValue = true;
            if (!readyForVerification)
                throw new ApplicationException("Commitments: not ready to verify (did you specify the data to verify?)");
            foreach (var i in everyCommitment)
            {
                if (!i.Verify())
                    returnValue=false;
            }
            return returnValue;
        }
        #endregion
    }
    #endregion
}
