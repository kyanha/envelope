﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Security;

namespace ClericalStorage
{
    public static class KeyStore
    {
        public static IEnumerable<Chop> ChopsHeld =
            from chop in myListOfChops where chop.privateKey != null select chop;
        public static IEnumerable<AsymmetricKeyParameter> KnownPublicKeys =
            from chop in myListOfChops select chop.publicKey;
        public static IEnumerable<byte[]> KnownPublicKeyIDs =
            from chop in myListOfChops select chop.keyIdentifier;

        public static List<Chop> myListOfChops = new List<Chop>();

        static KeyStore()
        {
            // Need to load the KeyStore with all known public keys.
        }

        /// <summary>
        /// Finds a public key given its keyIdentifier
        /// </summary>
        /// <param name="keyIdentifier">The keyIdentifier to search for</param>
        /// <returns></returns>
        public static Chop FindKnownVerifier(byte[] keyIdentifier)
        {
            IEnumerable<Chop> Find =
                from chop in myListOfChops where chop.keyIdentifier == keyIdentifier select chop;
            if (Find.Count<Chop>() > 1)
                throw new Org.BouncyCastle.Security.GeneralSecurityException(
                    "KeyIdentifier duplicated");
            foreach (var i in Find)
                return i;
            throw new Org.BouncyCastle.Security.SecurityUtilityException(
                "could not find Chop for keyIdentifier " + Convert.ToBase64String(keyIdentifier).Replace('+','-'));
        }

        public static Chop GetNewChop()
        {
            IAsymmetricCipherKeyPairGenerator _kpgen = new ECKeyPairGenerator("EC");
            _kpgen.Init(new KeyGenerationParameters(new SecureRandom(new Org.BouncyCastle.Crypto.Prng.CryptoApiRandomGenerator()), 256));
            Console.Write(":keygeneration");
            var myChop = new Chop(_kpgen.GenerateKeyPair());
            myListOfChops.Add(myChop);
            return myChop;
        }
    }
}
