﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Asn1;
using System.IO;

namespace ClericalStorage
{
    /// <summary>
    /// Everything needed to use and reference one Assertion (user datum)
    /// </summary>
    /// <remarks>
    /// An Assertion has one and only one Stream associated with it.
    /// It must have one and only one AssertionType.
    /// It has one (optional) name.
    /// It has one (optional) internal identifier.
    /// Assertion ::= SEQUENCE {
    ///     assertionInternalID INTEGER OPTIONAL,
    ///     assertionName UTF8String OPTIONAL,
    ///     assertionType [7] AssertionType,
    ///     assertionContent OCTET STRING }
    /// </remarks>
    public class Assertion:Deserializable
    {
        #region internal members
        int? assertionInternalID = null;
        string assertionName = null;
        public AssertionType assertionType;
        System.IO.Stream assertionContent = new System.IO.MemoryStream();
        Payload payloadIExistWithin; // If null, true == (this is Stamp)
        Envelope envelopeIExistWithin;

        // Only used if this is an x509-certificate
        public bool? unverified { get; internal set; }

        #endregion

        #region main constructor
        public Assertion(Stream input = null, byte[] input2 = null, AssertionType type = null,
            string AssertionName = null, int? AssertionInternalID = null)
        {
            unverified = true;
            // This isn't from a preserialized Envelope structure
            fromEnvelope = false;
            assertionContent = new MemoryStream();
            if (input == null && input2 != null)
                assertionContent = new MemoryStream(input2);
            else if (input != null)
                assertionContent = input;
            else
                assertionContent = new MemoryStream();
            this.assertionInternalID = AssertionInternalID;
            assertionName = AssertionName;
            if (type == null)
                assertionType = new AssertionType(type: "application/octet-string");
            else
                assertionType = type;
        }
        #endregion
        #region Convenience constructors
        public Assertion() : this(null, null, null, null, null) { }

        public Assertion(Stream stream, AssertionType assertionType) :
            this(stream, null, assertionType, null, null) { }

        public Assertion(Stream datastream) : this(datastream, null, null, null, null) { }
        public Assertion(byte[] sourceArray) : this(null, sourceArray, null, null, null) { }
        #endregion
        #region Deserialization constructor
        public Assertion(Asn1Encodable value) : base(value)
        {
            Deserialize(value);
        }

        public Assertion(Asn1Encodable value, Payload myPayload): this(value)
        {
            payloadIExistWithin = myPayload;
            envelopeIExistWithin = myPayload.myEnvelope;
        }

        public override void Deserialize(Asn1Encodable value)
        {
            var sequence = Asn1Sequence.GetInstance(value);
            if (sequence == null)
                throw new ArgumentException("Assertion: expected a sequence");
            if (sequence.Count < 2 || sequence.Count > 4)
                throw new Org.BouncyCastle.Security.Certificates.CertificateException(
                    "Assertion: sequence count " + sequence.Count + " not okay (2 <= count <= 4)");
            var myList = new List<object>();
            foreach (var n in sequence)
                myList.Add(n);

            var internalID = DerInteger.GetInstance(myList[0]);
            assertionInternalID = (internalID == null ? (int?)null:internalID.Value.IntValue);
            if (assertionInternalID != null)
                myList.RemoveAt(0);
            var name = DerUtf8String.GetInstance(myList[0]);
            if (name != null)
            {
                myList.RemoveAt(0);
                this.assertionName = name.GetString(); //name.Value;
            }
            var assertionT = DerTaggedObject.GetInstance(myList[0]);
            if (assertionT != null)
                myList.RemoveAt(0);
            else
                throw new Org.BouncyCastle.Security.Certificates.CertificateEncodingException(
                    "Assertion: expected explicitly tagged object, got " + myList[0] + "instead");
            // AssertionType can be either a MIME-type or an OID
            assertionType = new AssertionType(assertionT);
            var content = DerOctetString.GetInstance(myList[0]);
            if (content == null)
                throw new Org.BouncyCastle.Security.Certificates.
                    CertificateException("Assertion: final element not OCTET STRING");
            assertionContent = content.GetOctetStream();
            myList.RemoveAt(0);
            System.Diagnostics.Debug.Assert(myList.Count == 0);

            if (assertionType.mimeType == "application/x509-certificate")
            {
                envelopeIExistWithin.AddRequiredProof(this);
            }
        }
#endregion
        #region SetType (mandatory)
        /// <summary>
        /// Set the type of the Assertion to a MIME-type or string-form OID
        /// </summary>
        /// <param name="mimeType">MIME-type or string-form OID indicating assertionContent'outputStream type</param>
        public void SetType(AssertionType kind)
        {
            assertionType = kind;
        }
        #endregion
        #region SetContent (mandatory)
        public void SetContent(Stream input)
        {
            assertionContent = new MemoryStream();
            input.CopyTo(assertionContent);
        }
        #endregion
        #region SetName (optional)
        /// <summary>
        /// Set name of the Assertion
        /// </summary>
        /// <param name="assertionName">the logical filename of the assertionContent</param>
        /// <remarks>
        /// This causes Assertion to lose reference to its old name.
        /// </remarks>
        public void SetName(string assertionName)
        {
            this.assertionName = assertionName;
        }
        #endregion
        #region Serialize() and ToAsn1Encodable()
        internal System.IO.Stream Serialize()
        {
            // verify we have a type and a stamp
            if (assertionType == null || assertionContent == null)
                throw new InvalidOperationException("require assertionType and assertionContent");

            // Generate the DER sequence
            var outstream = new System.IO.MemoryStream();
            var sequence = new Org.BouncyCastle.Asn1.DerSequenceGenerator(outstream);
            if (assertionInternalID != null)
                sequence.AddObject(new DerInteger((int)assertionInternalID));
            if (assertionName != null)
                sequence.AddObject(new DerUtf8String(assertionName));
            sequence.AddObject(
                new DerTaggedObject(7, assertionType.ToAsn1Encodable()));
            sequence.AddObject(new DerOctetString(assertionContent.ToByteArray()));
            sequence.Close();
            // Rewind the Stream
            outstream.Position = 0;
            return outstream;
        }

        virtual public Asn1Encodable ToAsn1Encodable()
        {
            return Asn1Sequence.FromStream(Serialize());
        }
        #endregion

        internal void SetEnvelope(Envelope myEnvelope)
        {
            envelopeIExistWithin = myEnvelope;
        }
    }
}
