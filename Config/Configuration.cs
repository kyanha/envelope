﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Crypto.Digests;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Security;
using ClericalStorage;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.Nist;

namespace ClericalStorage.Config
{
    /// <summary>
    /// Holds runtime behavior configuration information
    /// </summary>
    public static class Configuration
    {
        /// <summary>
        /// Should Verify() throw if it fails?
        /// </summary>
        public static bool throwOnFailedVerify = false;

        /// <summary>
        /// The digest algorithm to use for creating keyIdentifier (max size 384 bits or 48 bytes)
        /// </summary>
        public static IDigest keyIdentifierDigest =
            Org.BouncyCastle.Security.DigestUtilities.GetDigest("SHA384");

        /// <summary>
        /// The list of digests to use for Commitments
        /// </summary>
        public static IList<DerObjectIdentifier> commitmentsDigests = new List<DerObjectIdentifier>
        {
            // GOST R3411
            Org.BouncyCastle.Asn1.CryptoPro.CryptoProObjectIdentifiers.GostR3411,
            // RIPE MD256
            Org.BouncyCastle.Asn1.TeleTrust.TeleTrusTObjectIdentifiers.RipeMD256,
            // MD5 (here for illustrative purposes)
            Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers.MD5,
            // MD2 (also here for illustrative purposes)
            Org.BouncyCastle.Asn1.Pkcs.PkcsObjectIdentifiers.MD2,
            // SHA-1
            Org.BouncyCastle.Asn1.X509.X509ObjectIdentifiers.IdSha1,
            // SHA-2 family
            Org.BouncyCastle.Asn1.Nist.NistObjectIdentifiers.IdSha384, 
        };

        /// <summary>
        /// The digest algorithms to use with EC chops
        /// </summary>
        public static readonly List<String> ecChopProofDigestAlgorithms = new List<string>{ "SHA1", "SHA384", "RIPEMD160", "GOST3411", };

        /// <summary>
        /// The digest algorithms to use with RSA chops
        /// </summary>
        public static readonly List<String> rsaChopProofDigestAlgorithms = new List<string>{ "SHA1", "SHA384", "SHA512", "RIPEMD160", "GOST3411", };

        /// <summary>
        /// These are the types of Assertions which are added to requiredProofs
        /// </summary>
        public static IEnumerable<AssertionType> provableAssertionTypes =
            new List<AssertionType> { 
                new AssertionType("application/x509-certificate"),
                //new AssertionType("application/openpgp-public-key")
            };

        /// <summary>
        /// The signature algorithm to use for the outermost Certificate
        /// </summary>
        public static string outerEnvelopeSignatureAlgorithm = "SHA384WITHECDSA";

        /// <summary>
        /// If we need to ECDH, we must use a block cipher to encrypt the LetterOpeners
        /// </summary>
        public static DerObjectIdentifier LetterOpenerBlockCipher = NistObjectIdentifiers.IdAes192Cfb;
        
        /// <summary>
        /// If we need to ECDH, we must use a block cipher to encrypt the Payload
        /// </summary>
        public static DerObjectIdentifier OpaquePayloadBlockCipher = NistObjectIdentifiers.IdAes192Cfb;
    }
}
