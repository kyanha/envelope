﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ClericalStorage
{
    /// <summary>
    /// Individual piece of certificate validity information
    /// </summary>
    /// <remarks>
    /// ValidityDatum ::= Assertion
    /// 
    /// Of course, this is normally an OCSP response.
    /// 
    /// This has been moved out of Assertions because it
    /// needs to be updated much more often than the actual
    /// contents of the ACContainer.  (Requiring the application of
    /// high-value keys every hour makes them that much more vulnerable.
    /// Better to let the OCSP be added separately so that the current
    /// status of the signing entity certificates can be continually
    /// refreshed.)
    /// </remarks>
    class ValidityDatum : Assertion
    {
        public ValidityDatum(
            Org.BouncyCastle.Asn1.Asn1Encodable validityDatumSequence)
            : base(validityDatumSequence)
        { }

        public ValidityDatum(Stream input, byte[] input2, AssertionType type,
            string AssertionName, int? AssertionInternalID)
            : base(input, input2, type, AssertionName, AssertionInternalID)
        {
            if (type == null)
                assertionType = new AssertionType(
                    type: "application/ocsp-response");
            System.Diagnostics.Debug.Assert(
                assertionType.ToString() == "application/ocsp-response" ||
                assertionType.ToString() == "application/x509-crl");
        }
    }
}
