﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Asn1;
using ClericalStorage;

namespace ClericalStorage
{
    /// CopyrightLicense ::= SEQUENCE(0..MAX) OF LicenseClause
    /// LicenseClause ::= SEQUENCE {
    ///     parsableCopyrightTerm OBJECT IDENTIFIER ,
    ///     languageOfControl UTF8String OPTIONAL ,
    ///     legibleCopyrightText LegibleClauses OPTIONAL }
    /// LegibleClauses ::= SEQUENCE(0..MAX) OF LegibleClause
    /// LegibleClause ::= SEQUENCE {
    ///     languageCode UTF8String,
    ///     textOfLicense UTF8String }

    ///<summary>Individual copyright clause, with an OID and a set of clause translations</summary>

    public class LicenseClause:Deserializable
    {
        #region Public Static members

        public static IDictionary<DerObjectIdentifier, LicenseClause> allPossibleClauses =
            new Dictionary<DerObjectIdentifier, LicenseClause>();
        
        #endregion
        #region public members
        
        public Org.BouncyCastle.Asn1.DerObjectIdentifier myOID { get; private set; }
        public string languageOfControl { get; private set; }
        public LegibleClauses legibleClauses;
        
        #endregion

        public LicenseClause(string OID)
        {
            myOID = new Org.BouncyCastle.Asn1.DerObjectIdentifier(OID);
            legibleClauses = TheClerkLegibleClauses.dictionaryOfLegibleClauses[myOID];
            // TODO: Make this configurable
            languageOfControl = "en/US";
        }

        public LicenseClause(Asn1Encodable value) : base(value)
        {
            Deserialize(value);
        }

        public void Add(LegibleClause c)
        {
            legibleClauses.Add(c);
        }

        internal Stream Serialize()
        {
            MemoryStream s = new MemoryStream();
            var dsg = new DerSequenceGenerator(s);
            // Object Identifier
            dsg.AddObject(myOID);
            // languageOfControl
            if (null != languageOfControl)
                dsg.AddObject(new Org.BouncyCastle.Asn1.DerUtf8String(languageOfControl));
            foreach (LegibleClause i in legibleClauses)
            {
                dsg.AddObject(i.ToAsn1Encodable());
            }
            dsg.Close();
            s.Position = 0;
            return s;
        }

        internal byte[] AsByteArray()
        {
            var s = Serialize();
            byte[] buf = new byte[s.Length];
            s.Read(buf, 0, buf.Length);
            return buf;
        }

        internal Asn1Encodable ToAsn1Encodable()
        {
            return Asn1Object.FromStream(Serialize());
        }

        /// LicenseClause ::= SEQUENCE {
        ///     parsableCopyrightTerm OBJECT IDENTIFIER ,
        ///     languageOfControl UTF8String OPTIONAL ,
        ///     legibleCopyrightText LegibleClauses OPTIONAL }
        public override void Deserialize(Asn1Encodable value)
        {
            var A = Asn1Sequence.GetInstance(value);
            if (null == A)
            {
                throw new ArgumentException("LicenseClause: Expected a sequence");
            }
            if (A.Count < 0 || A.Count > 3)
            {
                throw new ArgumentException("LicenseClause: expected a sequence between 1 <= n <= 3");
            }
            var B = new System.Collections.Generic.List<Asn1Encodable>();
            for (int C = 0; C < A.Count; C++)
            {
                B[C] = A[C];
            }
            myOID = DerObjectIdentifier.GetInstance(B[0]);
            B.RemoveAt(0);
            if(B.Count == 0)
                return;
            languageOfControl = DerUtf8String.GetInstance(B[0]).GetString();
            if (languageOfControl != null)
            {
                B.RemoveAt(0);
            }
            if (B.Count > 0)
                legibleClauses = new LegibleClauses(B[0]);

            if(languageOfControl == null)
                languageOfControl="en/US";
            if (legibleClauses == null)
                legibleClauses = new LegibleClauses();
            System.Diagnostics.Debug.Assert(languageOfControl != null);
            System.Diagnostics.Debug.Assert(legibleClauses != null);

            // TODO If languageOfControl or legibleClauses weren't set, we must look them up.
        }
    }
}
