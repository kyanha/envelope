﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
// Bring extension methods into scope
using ClericalStorage;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.X509;

namespace ClericalStorage
{
    /// <summary>
    /// Contains methods to interact with the underlying implementation
    /// </summary>
    public static class PlatformInterface
    {
        internal static byte[] ToByteArray(this System.IO.Stream input)
        {
            var output = new byte[input.Length];
            input.Position = 0;
            input.Read(output, 0, checked((int)input.Length));
            return output;
        }

        internal static DerObjectIdentifier OID(this string value)
        {
            return new DerObjectIdentifier(value);
        }

        public static List<Org.BouncyCastle.Tsp.TimeStampResponse> nullTimeStampResponses
            = new List<Org.BouncyCastle.Tsp.TimeStampResponse>();
        public static List<Org.BouncyCastle.Tsp.TimeStampToken> TimeStampTokens
            = new List<Org.BouncyCastle.Tsp.TimeStampToken>();
    }
}
