﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClericalStorage
{
    /// <summary>
    /// One or more ValidityDatum
    /// </summary>
    /// <remarks>
    /// ValidityAssertions ::= SEQUENCE OF ValidityDatum 
    /// ValidityDatum ::= SEQUENCE {
    ///    datumType OBJECT IDENTIFIER,
    ///    datum ANY -- DEFINED BY datumType -- }
    /// </remarks>
    /// kyanha: can this be Assertions or Stamps?
    class ValidityAssertions : Assertions
    {
        private ISet<ValidityDatum> setOfValidityDatum;

        public ValidityAssertions(Org.BouncyCastle.Asn1.Asn1Encodable validityDataSequence)
        {
            // This should be a SEQUENCE
            var seq = Org.BouncyCastle.Asn1.Asn1Sequence.GetInstance(validityDataSequence);
            if (seq == null)
                throw new Org.BouncyCastle.Security.Certificates.CertificateParsingException("ValidityAssertions: not a sequence");
            setOfValidityDatum = new System.Collections.Generic.HashSet<ValidityDatum>();
            foreach (Org.BouncyCastle.Asn1.Asn1Encodable item in seq)
            {
                setOfValidityDatum.Add(new ValidityDatum(item));
            }
        }

        public ValidityAssertions()
        {
            setOfValidityDatum = new HashSet<ValidityDatum>();
        }

        
    }
}
