﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Crypto;

namespace ClericalStorage
{
    /// <summary>
    /// The container for AssertionCommitment (userdata + digests) and Proofs
    /// </summary>
    /// <remarks>
    /// EnvelopeContent ::= SEQUENCE {
    /// 	signedData ACContainer,
    ///     proofs Proofs }
    ///     
    /// This class is responsible for using the Chops to sign
    /// the serialized AssertionCommitment.
    /// </remarks>
    public class EnvelopeContent : Deserializable
    {
        #region public members and constructors
        public ACContainer signedData;
        public Proofs proofs;
        public List<AsymmetricIdentity> chopsRequired = new List<AsymmetricIdentity>();
        private List<Chop> chops = new List<Chop>();
        private Org.BouncyCastle.Asn1.Asn1Encodable asn1Encodable;
        public byte[] acOctetVector;
        public Stream acStream;

        public EnvelopeContent()
        {
            signedData = new ACContainer();
            proofs = new Proofs(signedData);
        }

        public EnvelopeContent(Org.BouncyCastle.Asn1.Asn1Encodable asn1Encodable)
        {
            Deserialize(asn1Encodable);
        }

        public override void Deserialize(Org.BouncyCastle.Asn1.Asn1Encodable asn1Encodable)
        {
            // TODO: Complete member initialization
            this.asn1Encodable = asn1Encodable;
            var sequence = Asn1Sequence.GetInstance(asn1Encodable);
            if (sequence.Count != 2)
                throw new Org.BouncyCastle.Security.Certificates.
                    CertificateException("EnvelopeContent count " + sequence.Count + " not 2");
            signedData = new ACContainer(sequence[0]);
            proofs = new Proofs(sequence[1]);
        }
        #endregion

        public Stream Serialize()
        {
            // Build the AssertionCommitment so that it can be signed
            acOctetVector = signedData.ToByteArray();
            // Now, construct the Proofs...
            proofs.SetACContent(acOctetVector);

            // and create the new Asn1Sequence
            var dsg = new Org.BouncyCastle.Asn1.DerSequenceGenerator(acStream);
            // ACContainer goes in first
            dsg.AddObject(new Org.BouncyCastle.Asn1.DerOctetString(acOctetVector));

            // and serialize it to a byte array
            // byte[] Pder = proofs.ToByteArray();
            dsg.AddObject(proofs.ToAsn1Encodable());
            dsg.Close();
            acStream.Position = 0;
            return acStream;
        }

        public byte[] ToByteArray()
        {
            var s = Serialize();
            var buf = new byte[s.Length];
            s.Read(buf, 0, buf.Length);
            return buf;
        }

        public void AddAssertion(Assertion content)
        {
            signedData.AddAssertion(content);
        }

        public void AddChop(Chop equipment)
        {
            chops.Add(equipment);
            proofs.AddChop(equipment);
        }

        public Org.BouncyCastle.Asn1.Asn1Encodable ToAsn1Encodable()
        {
            return Org.BouncyCastle.Asn1.Asn1Object.FromStream(Serialize());
        }

        internal void AddRequiredProof(Assertion input)
        {
            this.proofs.AddRequiredProof(input);
        }

        public Envelope myEnvelope
        {
            get { return this.signedData.myEnvelope; }
            internal set { this.signedData.myEnvelope = value; }
        }
    }
}
