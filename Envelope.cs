﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.X509;

namespace ClericalStorage
{
    /// <summary>
    /// The Envelope structure itself
    /// </summary>
    /// <remarks>
    /// Envelope is an X.509 Certificate structure with 4 extensions.
    /// OpaquePayload
    ///     (generated from {Payload}, generated from { Assertions, Commitments, Proofs, Postmarks } )
    /// StrongBox
    ///     (set of StrongBoxItem)
    /// Stamps
    ///     (set of Stamp : Assertion)
    /// CopyrightLicense
    ///     (natural-language)
    ///     (parsable)
    /// Envelope must also be equipped with all the chops before it can seal.
    /// Envelope must create StrongBox and pass it to OpaquePayload constructor
    /// </remarks>
    public class Envelope
    {
        // Our certificate generator
        X509V3CertificateGenerator envelopeGenerator;

        // The four main extensions potentially in this structure
        OpaquePayload opaquePayload;
        StrongBox strongBox;
        Stamps stamps;
        CopyrightLicense License;

        public IList<Assertion> proofsRequired = new List<Assertion>();

        /// <summary>
        /// Constructs a completely empty Envelope
        /// </summary>
        public Envelope()
        {
            // We need to send OpaquePayload the StrongBox this Envelope uses
            // There is only one circumstance where the strongbox isn't needed,
            // and it's possible that it'll be needed even then.
            strongBox = new StrongBox();
            opaquePayload = new OpaquePayload(strongBox);
            opaquePayload.myEnvelope = this;
            stamps = new Stamps();
            License = new CopyrightLicense();
        }

        /// <summary>
        /// Add a private key to the list of Signers
        /// </summary>
        /// <param name="equipment">the chop to equip</param>
        public void EquipChop(Chop equipment)
        {
            opaquePayload.AddChop(equipment);
        }

        /// <summary>
        /// Add a public key to the list of Recipients to have a LetterOpener made
        /// </summary>
        /// <param name="recipient"></param>
        public void AddRecipient(AsymmetricIdentity recipient)
        {
            opaquePayload.AddRecipient(recipient);
            strongBox.AddRecipient(recipient);
        }

        /// <summary>
        /// Add transparent, shell-visible data to the Envelope
        /// </summary>
        /// <param name="stamp">The data to add transparently</param>
        /// <remarks>
        /// This information must be considered more suspect than Assertions,
        /// as it's protected by only a single signature under a single
        /// algorithm which (pessimistically) will be broken at some
        /// point in the future.  This means that it's not necessarily
        /// the best place for unverifiable information.  However, this is
        /// also the only location where the client can place information
        /// that is guaranteed to be transparent.
        /// </remarks>
        public void AddStamp(object stamp)
        {
            dynamic mystamp = null;
            System.Diagnostics.Debug.Assert(
                stamp is byte[] ||
                (stamp is Stream && (stamp as Stream).CanRead) ||
                stamp is Assertion);
            if (stamp is Assertion)
                mystamp = stamp;
            else if (stamp is byte[])
                mystamp = new Assertion(stamp as byte[]);
            else if (stamp is Stream && (stamp as Stream).CanRead)
                mystamp = new Assertion(stamp as Stream);
            else
                throw new InvalidDataException("Stamp: requires Assertion, readable Stream, or byte[]");

            this.stamps.Add(stamp as Assertion);
            return;
        }

        /// <summary>
        /// Add stamp either as an Assertion, byte[], or Stream
        /// </summary>
        /// <param name="stamp">Must be Assertion, byte[], or readable
        /// System.IO.Stream</param>
        public void AddPayload(object content)
        {
            System.Diagnostics.Debug.Assert(
                content is byte[] || 
                (content is Stream && (content as Stream).CanRead) ||
                content is Assertion);

            if (content is Assertion)
            {
                this.opaquePayload.payload.AddAssertion(content as Assertion);
                return;
            }
            if (content is Stream && true == (content as Stream).CanRead)
            {
                // It's not an Assertion, so we need to make one out of it
                this.opaquePayload.payload.AddAssertion(new Assertion(content as Stream));
                return;
            }
            if (content is byte[])
            {
                this.opaquePayload.payload.AddAssertion(new Assertion(content as byte[]));
                return;
            }
            throw new InvalidDataException("Envelope: invalid object passed as payload");
        }

        /// <summary>
        /// Seal it all up, returns a byte[]
        /// </summary>
        /// <returns></returns>
        public byte[] Seal()
        {
            envelopeGenerator = new X509V3CertificateGenerator();
            // Generi-fill the static fields of the Envelope
            fillEnvelope();

            // We need the nonce Chop at this point.
            Chop myChop = KeyStore.GetNewChop();
            opaquePayload.SetOuterChop(myChop);
            
            // If the same IssuerDN issues the same serial number multiple times
            // and the endpoint knows it, e.g. Mozilla refuses to talk.
            // We solve it by using the Envelope key's digest as a Pseudonym=,
			// which causes every certificate to be self-issued by a different
			// Issuer, without undue risk of serial number collisions.  We also
			// set the serial number with 159 bits (20 bytes with a 20-byte DER
			// encoding) of cryptorandom data, which reduces the risk of collision
			// that much more.

            // Pseudonym has a maximum size of 64 ASCII characters, which means
			// that we need a 384 bit or smaller digest to base64-encode into it.
			// FIXME I'm hardcoding SHA384 here.

			var digest = new Org.BouncyCastle.Crypto.Digests.Sha384Digest();

            // This math works out to 48, for base64 3->4 expansion
            if (digest.GetByteLength() > (64 / 4 * 3))
                throw new Org.BouncyCastle.Security.InvalidParameterException(
                    "Envelope: Insufficient space in Pseudonym for keyIdentifier of length "
                    + digest.GetByteLength()+ ", max is 48 bytes or 384 bits");
            byte[] output = new byte[digest.GetByteLength()];
            digest.Reset();
            byte[] spki = SubjectPublicKeyInfoFactory.CreateSubjectPublicKeyInfo(myChop.publicKey)
                .GetDerEncoded();
            digest.BlockUpdate(spki,0,spki.Length);
            digest.DoFinal(output, 0);

            envelopeGenerator.SetPublicKey(myChop.publicKey);

            var pseudoName = new X509Name("Pseudonym=" + System.Convert.ToBase64String(output).Replace('+','-'));
            envelopeGenerator.SetIssuerDN(pseudoName);
            envelopeGenerator.SetSubjectDN(pseudoName);

            // Extensions get created and serialized here
            // OpaquePayload
            if (opaquePayload.Valid)
                envelopeGenerator.AddExtension(
                    oid: new DerObjectIdentifier("1.3.6.1.4.1.22232.7.1.0.0"),
                    critical: false,
                    extensionValue: new DerOctetString(this.opaquePayload.AsByteArray()));
            // StrongBox
            if (strongBox.Valid)
                envelopeGenerator.AddExtension(
                    oid: new DerObjectIdentifier("1.3.6.1.4.1.22232.7.1.0.1"),
                    critical: false,
                    extensionValue: strongBox.ToAsn1Encodable().GetDerEncoded());
            // Copyright Notices
            if (License.Valid)
                envelopeGenerator.AddExtension(
                    oid: new DerObjectIdentifier("1.3.6.1.4.1.22232.7.1.0.2"),
                    critical: true,
                    extensionValue: this.License.ToAsn1Encodable().GetDerEncoded());
            // Stamps
            if (stamps.Valid)
                envelopeGenerator.AddExtension(
                    oid: new DerObjectIdentifier("1.3.6.1.4.1.22232.7.1.0.3"),
                    critical: false,
                    extensionValue: stamps.ToAsn1Encodable().GetDerEncoded());

            return envelopeGenerator.Generate(myChop.privateKey).GetEncoded();
        }

        /// <summary>
        /// Obtain the sealed Envelope as a Stream
        /// </summary>
        /// <returns>Stream containing sealed Envelope</returns>
        public Stream Serialize() { return new MemoryStream(Seal()); }

        public Asn1Encodable ToAsn1Encodable()
        {
            return Asn1Object.FromStream(Serialize());
        }

        /// <summary>
        /// Called by Seal() to get the generator set up
        /// </summary>
        private void fillEnvelope()
        {
            envelopeGenerator.SetNotBefore(DateTime.UtcNow);
            envelopeGenerator.SetNotAfter(DateTime.UtcNow.AddYears(1));
            envelopeGenerator.SetSerialNumber(
                new Org.BouncyCastle.Math.BigInteger(
                    (20 * 8 - 1), // 20 bytes times 8 is 160 bits, less 1 bit to prevent 21-byte encoding
                                  // 20 bytes is the output of SHA1, and PKIX minimum largest acceptable
                    new Org.BouncyCastle.Security.SecureRandom(
                        new Org.BouncyCastle.Crypto.Prng.CryptoApiRandomGenerator())));
            envelopeGenerator.SetSignatureAlgorithm(Config.Configuration.outerEnvelopeSignatureAlgorithm);
        }

        internal void Add(Assertion a)
        {
            opaquePayload.AddAssertion(a);
        }

        #region Constructor from byte[] (i.e., deserialize)
        public Envelope(byte[] value)
        {
            var x509parser = new Org.BouncyCastle.X509.X509CertificateParser();
            var envelopecert = x509parser.ReadCertificate(value);
            var a = envelopecert.CertificateStructure.TbsCertificate.Extensions.
                ExtensionOids.Cast<DerObjectIdentifier>();
            if(!(a.Contains(TheClerkObjectIdentifiers.dictionaryObjectIDs["idOpaquePayload"]) ||
                a.Contains(TheClerkObjectIdentifiers.dictionaryObjectIDs["idStamps"])))
                    throw new InvalidDataException("not an Envelope");
            try
            {
                envelopecert.Verify(envelopecert.GetPublicKey());
                if(a.Contains(TheClerkObjectIdentifiers.dictionaryObjectIDs["idOpaquePayload"]))
                    this.opaquePayload = new OpaquePayload(
                        envelopecert.GetExtensionValue(
                        TheClerkObjectIdentifiers.dictionaryObjectIDs["idOpaquePayload"]).GetOctets());
                if (a.Contains(TheClerkObjectIdentifiers.dictionaryObjectIDs["idStrongBox"]))
                    this.strongBox = new StrongBox(
                        envelopecert.GetExtensionValue(
                        TheClerkObjectIdentifiers.dictionaryObjectIDs["idStrongBox"]).GetOctets());
                if (a.Contains(TheClerkObjectIdentifiers.dictionaryObjectIDs["idCopyrightLicense"]))
                    this.License = new CopyrightLicense(
                        envelopecert.GetExtensionValue(
                        TheClerkObjectIdentifiers.dictionaryObjectIDs["idCopyrightLicense"]).GetOctets());
                if (a.Contains(TheClerkObjectIdentifiers.dictionaryObjectIDs["idStamps"]))
                    this.stamps = new Stamps(
                        envelopecert.GetExtensionValue(
                        TheClerkObjectIdentifiers.dictionaryObjectIDs["idStamps"]).GetOctets());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                System.Diagnostics.Debugger.Break();
            }
        }
        #endregion

        internal void AddRequiredProof(Assertion input)
        {
            this.proofsRequired.Add(input);

            // FIXME This should also remove the IssuerKeyIdentifier from proofsRequired

            opaquePayload.AddRequiredProof(input);
        }


    }
}
