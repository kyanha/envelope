﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClericalStorage
{
    interface IAddPayload
    {
        void AddPayload(object payload);
    }
}
