﻿using System;
using System.Collections.Generic;
using Org.BouncyCastle.Asn1;

namespace ClericalStorage
{
    static class TheClerkLegibleClauses
    {
        /// <summary>
        /// The dictionary which holds the OID to localized legible clause containers
        /// </summary>
        public static readonly Dictionary<DerObjectIdentifier, LegibleClauses> dictionaryOfLegibleClauses;

        // TODO: Make Legible Clauses according to more than just "All Rights Reserved"
        static TheClerkLegibleClauses()
        {
            dictionaryOfLegibleClauses = new Dictionary<DerObjectIdentifier, LegibleClauses>();
            var s = new LegibleClauses();
            s.Add(new LegibleClause("en/US", "All Rights Reserved"));
            dictionaryOfLegibleClauses.Add(TheClerkObjectIdentifiers.dictionaryObjectIDs["idLicenseAllRightsReserved"], s);
        }
    }

    /// <remarks>
    /// A class to hold object identifiers
    /// </remarks>
    static class TheClerkObjectIdentifiers
    {
        static readonly DerObjectIdentifier idTheClerk = new DerObjectIdentifier("1.3.6.1.4.1.22232.7");
        static readonly DerObjectIdentifier idForm = new DerObjectIdentifier(idTheClerk + ".1");
        static readonly DerObjectIdentifier idCopyrightLicense = new DerObjectIdentifier(idForm + ".3");
        static readonly DerObjectIdentifier idEnvelope = new DerObjectIdentifier(idForm + ".1");
        static readonly DerObjectIdentifier idLicenseCreativeCommons = new DerObjectIdentifier(idCopyrightLicense + ".1");
        static readonly DerObjectIdentifier idLicenseCommercialInterest = new DerObjectIdentifier(idCopyrightLicense + ".2");

        /// <summary>
        /// The dictionary which holds the name to OID mappings
        /// </summary>
        public static readonly Dictionary<string, DerObjectIdentifier> dictionaryObjectIDs =
            new Dictionary<string, DerObjectIdentifier>();

        static TheClerkObjectIdentifiers()
        {
            dictionaryObjectIDs["idTheClerk"] = new DerObjectIdentifier(idTheClerk.ToString());
            dictionaryObjectIDs["idForm"] = new DerObjectIdentifier(idTheClerk + ".1");
            dictionaryObjectIDs["idEnvelope"] = new DerObjectIdentifier(idForm + ".1");
            dictionaryObjectIDs["idOpaquePayload"] = new DerObjectIdentifier(idEnvelope + ".1");
            dictionaryObjectIDs["idStrongBox"] = new DerObjectIdentifier(idEnvelope + ".2");
            dictionaryObjectIDs["idCopyrightLicense"] = new DerObjectIdentifier(idForm + ".3");
            dictionaryObjectIDs["idLicenseAllRightsReserved"] = new DerObjectIdentifier(idCopyrightLicense + ".0");
            dictionaryObjectIDs["idLicenseCreativeCommons"] = new DerObjectIdentifier(idCopyrightLicense + ".1");
            dictionaryObjectIDs["idPD0"] = new DerObjectIdentifier(idLicenseCreativeCommons + ".0");
            dictionaryObjectIDs["idBY"] = new DerObjectIdentifier(idLicenseCreativeCommons + ".1");
            dictionaryObjectIDs["idBY_NC"] = new DerObjectIdentifier(idLicenseCreativeCommons + ".2");
            dictionaryObjectIDs["idBY_NC_ND"] = new DerObjectIdentifier(idLicenseCreativeCommons + ".3");
            dictionaryObjectIDs["idBY_ND"] = new DerObjectIdentifier(idLicenseCreativeCommons + ".4");
            dictionaryObjectIDs["idBY_SA"] = new DerObjectIdentifier(idLicenseCreativeCommons + ".5");
            dictionaryObjectIDs["idBY_NC_SA"] = new DerObjectIdentifier(idLicenseCreativeCommons + ".6");
            dictionaryObjectIDs["idLicenseCommercialInterest"] = new DerObjectIdentifier(idCopyrightLicense + ".2");
            dictionaryObjectIDs["idMayStore"] = new DerObjectIdentifier(idLicenseCommercialInterest + ".1");
            dictionaryObjectIDs["idMayContactPostal"] = new DerObjectIdentifier(idLicenseCommercialInterest + ".2");
            dictionaryObjectIDs["idMayContactPhone"] = new DerObjectIdentifier(idLicenseCommercialInterest + ".3");
            dictionaryObjectIDs["idMayContactEmail"] = new DerObjectIdentifier(idLicenseCommercialInterest + ".4");
            dictionaryObjectIDs["idMayContactForHirePostal"] = new DerObjectIdentifier(idLicenseCommercialInterest + ".5");
            dictionaryObjectIDs["idMayContactForHirePhone"] = new DerObjectIdentifier(idLicenseCommercialInterest + ".5");
            dictionaryObjectIDs["idMayContactForHireEmail"] = new DerObjectIdentifier(idLicenseCommercialInterest + ".6");
            dictionaryObjectIDs["idMayShareOrderInformation"] = new DerObjectIdentifier(idLicenseCommercialInterest + ".7");
            dictionaryObjectIDs["idMayShareWithThirdParty"] = new DerObjectIdentifier(idLicenseCommercialInterest + ".8");
            dictionaryObjectIDs["idStamps"] = new DerObjectIdentifier(idEnvelope + ".4");
        }
        /// <summary>
        /// The Null algorithm
        /// </summary>
        /// <remarks>
        /// Probably not used appropriately, but it is shorter than the encoding for kyanha's Private Enterprise OID.
        /// </remarks>
        public static readonly DerObjectIdentifier idNullAlgorithm = new DerObjectIdentifier("2.23.133.4.1");
    }
}
